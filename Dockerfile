FROM alpine

RUN apk --no-cache add \
    emacs-nox git pre-commit shellcheck

RUN ln -s /root/emacs-config/emacs.d /root/.emacs.d
