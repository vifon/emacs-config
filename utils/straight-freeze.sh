#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

export EMACS_NO_LOCAL=1

emacs -f straight-freeze-versions \
      --eval '(message "Version freeze complete")'
