#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

rm -v -rf ~/.emacs.d/straight/build-*
