#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

emacs --batch \
      -l ~/.emacs.d/early-init.el \
      -l ~/.emacs.d/init.el \
      -f straight-pull-all \
      -f straight-rebuild-all
