#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

rm -v -rf ~/.emacs.d/eln-cache/ ~/.local/share/flatpak/exports/share/emacs/native-lisp/
