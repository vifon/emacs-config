# Conditionally enable new features

## Context and Problem Statement

How should the relatively new Emacs features be enabled?

## Considered Options

* Enable everything unconditionally, expect the newest Emacs release
* Check `emacs-version` with `version<` or a similar function
* Check feature availability with `boundp` and `fboundp`

## Decision Outcome

Chosen option: "Check feature availability with `boundp` and
`fboundp`", because it's the most flexible solution.  For external
packages checking `emacs-version` is acceptable too.

## Pros and Cons of the Options

### Enable everything unconditionally, expect the newest Emacs release

* Good, because it requires no further actions
* Bad, because the new Emacs version availability heavily varies by system

### Check `emacs-version` with `version<` or a similar function

* Good, because older Emacs versions can be supported
* Good, because it's easy to determine when a conditional block exists
  only to support a now ancient version of Emacs and can be safely
  removed after a few years
* Bad, because backporting a feature won't make the config use it,
  even when available
* Good, because it doesn't rely on the symbol visibility during
  startup

### Check feature availability with `boundp` and `fboundp`

* Good, because older Emacs versions can be supported
* Bad, because it's not immediately clear how old the feature is and
  when the conditional load can be deprecated
  * Can be mitigated with additional documentation, for example
    a comment or a commit message trailer with the Emacs version
    providing this feature, added when it's still considered new
* Good, because the check relies solely on the feature being
  available, so if it's backported, it will be used as expected
* Bad, because some symbols are not autoloaded and are not visible
  during startup
