# Combine multiple Emacs code outline systems

## Context and Problem Statement

GNU Emacs ships with a few distinct code outlining and/or folding
systems, none enabled by default.  It isn't immediately clear which
one to use as each one has pros and cons.

## Decision Drivers

* Visual cues on the margin are highly desirable for quick discerning of the sections.
* Should work for both plain-text and source code.
* The folding should be technically correct (duh!) for a given syntax.
* It's desirable for the folding rules to be extensible and customizable.

## Considered Options

* `outline-minor-mode`, designed for plain-text documents
* `hideshow` / `hs-minor-mode`, designed for source code
* `selective-display`, the most basic of them, good as a fallback

## Decision Outcome

Chosen option: all three, each one for a different use case.

`outline-minor-mode` checks almost all the boxes. All except for
handling the source code syntax correctly!  While this is quite a huge
problem, `outline-minor-mode` can be used in conjunction with
`hideshow` to get the best of both worlds.

Since both of them require explicit major mode support (even if it's
widespread), `selective-display` is worth keeping in mind as
a fallback.

### Consequences

* Good, because most use cases are handled, including the long missing
  margin visual cues.
* Bad, because using a wrong folding mechanism for a given case might
  produce confusing results.
* Bad, because the rationale for this choice may become unclear with
  the passage of time.  Mitigated with this very document.
* Bad, because `outline-minor-mode` and `hideshow` have partially
  overlapping keymaps, necessitating explicit ordering of their
  activation in the hooks enabling them (see the `depth` argument of
  `add-hook`).

## Pros and Cons of the Options

### `outline-minor-mode`

* Good, because it provides great visual cues with
  `outline-minor-mode-use-buttons` set to `in-margins`.
* Good, because it's supported in most major modes I use.
* Good, because it's relatively easy to add support to unsupported
  modes (for example it's already done for YAML).
* Good, because handles plain-text great.
* Bad, because has no notion of ending a block without starting a new
  one (see below), which is necessary for handling source code.
  Works mostly okay for top-level elements.

A trivial Python example to demonstrate the problem:

```python
if this_block_is_being_folded():
    folded_as_expected()
this_is_mistakenly_folded_too()
if the_fold_ends_here():
    ...
```

Despite this flaw it seems worth using even just for the margin visual
cues for the top-level elements and some casual folding while keeping
this problem in mind.

### `hideshow` / `hs-minor-mode`

* Bad, because it provides no visual cues out of the box and the 3rd
  party packages adding them seem to be worse than what the 1st party
  `outline-minor-mode` offers.
* Good, because it's supported in most major modes I use.
* Neutral, because I'm yet to research its extensibility (no need yet).
* Bad, because it doesn't handle non-code at all.
* Good, because it handles source code properly.

### `selective-display`

`selective-display` doesn't really "fold blocks".  It instead hides
lines indented more than N levels.  The result is surprisingly
accurate for most programming languages.  Useful as a fallback when
the alternatives are unavailable.

* Bad, because it provides no visual cues.
* Bad, because it cannot be applied locally to a single block.
* Good, because it requires no major mode support to work.
* Neutral, the notion of extensibility isn't really applicable to it,
  but the way it works is inherently compatible with most
  programming languages.
* Bad, because it doesn't handle non-indented content at all,
  including most plain-text documents.
* Good, because it handles source code quite well for what it is.

## More Information

|                    | visual cues | good major mode support | extensibility | plain-text | source code |
|--------------------|:-----------:|:-----------------------:|:-------------:|:----------:|:-----------:|
| outline-minor-mode | X           | X                       | X             | X          | -           |
| hideshow           | -           | X                       | -             | -          | X           |
| selective-display  | -           | N/A                     | N/A           | -          | X           |
