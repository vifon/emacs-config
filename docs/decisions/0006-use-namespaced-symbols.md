# Use namespaced symbols

## Context and Problem Statement

For symbols defined in this config, should there be some common prefix used as a namespace?

## Decision Drivers

* It should be clear the symbol is defined in my config itself, not in Emacs, not in a 3rd party package
* It's especially important for custom advices to be visibly custom
* Symbols might be copied by other Emacs users into their own configs

## Considered Options

* No prefix
* A generic prefix, for example `my/symbol-name`
* A personalized prefix, for example `nickname/symbol-name`

## Decision Outcome

Chosen option: "A personalized prefix, for example `nickname/symbol-name`".

## Pros and Cons of the Options

### No prefix

* Bad, because it blends in with every external symbol

### A generic prefix, for example `my/symbol-name`

* Good, because custom symbols are immediately visible
* Bad, because custom symbols copied by the others look like their own
  custom code which might cause confusion in the future

### A personalized prefix, for example `nickname/symbol-name`

* Good, because custom symbols are immediately visible
* Good, because symbols can be safely copied by the others without
  looking like possibly their own code
