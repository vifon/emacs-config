# Avoid defining functions in the `use-package` `:config` section.

## Context and Problem Statement

Sometimes it's necessary to define a helper function when configuring
a package.  For example:

```elisp
(use-package example
  :config (defun my-command ()
            ...)
  :bind ("C-c c" . my-command))
```

It's assumed this function is tightly coupled with how the package is
used and is not referenced elsewhere.

Where should this function be defined?

## Decision Drivers

* When is the function getting defined?  Is the definition deferred
  (with `eval-after-load`) or is the function considered defined
  immediately after Emacs initialization?
* Is the function visually grouped with the code that references it?
* Is `xref` able to find the real function definition or is the
  `autoload` pointing at a wrong place?  What about the help buffers,
  do they point at the config file, the package file or at file
  at all?

## Considered Options

* Define the function in the `:init` section.
* Define the function in the `:config` section.
* Define the function outside of the `use-package` block.

## Decision Outcome

Chosen option: either "Define the function outside of the
`use-package` block" or "Define the function in the `:init` section".

Most importantly, never "Define the function in the `:config` section".

### Confirmation

Add a block like this to the config:

```elisp
(use-package example
  :bind (("C-c A" . my-test1)
         ("C-c B" . my-test2)
         ("C-c C" . my-test3))
  :init (defun my-test1 ()
          (interactive)
          (message "Hello world."))
  :config (defun my-test2 ()
            (interactive)
            (message "Hello world.")))

(defun my-test3 ()
  (interactive)
  (message "Hello world."))
```

(replace "example" with almost any already used package)

1. Start a new Emacs process.
2. Verify the package is no loaded yet, i.e. `(featurep 'example)` is `nil`.
3. Open all three defined functions in the help system (either `C-h f`
   or `C-h k`), check the file the help system refers to.

`my-test1` and `my-test3` are expected to be properly recognized as
defined in the config file.  `my-test2` is allegedly defined within
`example.el` until it's loaded and the implicit `eval-after-load`
block created with the `:config` section is evaluated.

Optionally try using `xref-find-definitions` (`M-.`) on references to
all three functions, but supposedly both systems follow the
same logic.

## Pros and Cons of the Options

### Define the function in the `:init` section.

* Good, the function location is properly reported.
* Good, the function is grouped with its only reference.
* Bad, the function is loaded before it's actually needed. (negligible)

### Define the function in the `:config` section.

While in theory the `:config` section sounds perfect on paper, the
fact it delays defining the function causes unpleasant side effects.

Referencing a yet-to-be-defined function in many `use-package`
sections implies the package defines it and creates an `autoload`
declaration pointing at that package.  Note that this faulty
`autoload` is ultimately harmless because loading this package loads
the `eval-after-load` block containing our function too.

On the other hand the cost of eagerly defining such a helper function
immediately on Emacs startup is negligible.  Possibly lower than the
code of involving the `autoload` system.

* Bad, the function location is not properly reported until loaded.
* Good, the function is grouped with its only reference.
* Good, the function is only loaded once it's needed. (negligible)

### Define the function outside of the `use-package` block.

* Good, the function location is properly reported.
* Bad, the function isn't clearly grouped with its only reference.
* Bad, the function is loaded before it's actually needed. (negligible)
