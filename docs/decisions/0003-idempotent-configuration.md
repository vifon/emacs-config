# The configuration should be idempotent

## Context and Problem Statement

Should it be expected for the config to work properly when loaded
multiple times?  I.e. is the config expected to be idempotent?

## Decision Drivers

* Common Emacs development workflows.
* Ease of development.
* Ease of debugging.

## Considered Options

* Make effort for the config to be idempotent.
* Don't care about idempotence, assume a fresh start.

## Decision Outcome

Chosen option: "Make effort for the config to be idempotent", because
it's extremely common to reload parts of the Emacs configuration
during development.  It makes iterative development and debugging
much easier.

### Consequences

* Good, because debugging is easier.
* Good, because iterative development doesn't need extra care as the
  whole config or its parts can be freely reloaded.
* Bad, because extra care is needed for each introduced change for the config to stay idempotent.

## More Information

Avoid appending to lists, instead prefer assigning them.
`add-to-list` is safe because it checks for the presence of the new
element and doesn't add extra copies.

If a specific file cannot be easily made idempotent, is should be
loaded with `require`, not `load`, so it's only loaded once.
Otherwise `load` is preferred to facilitate reloading instead of
silently doing nothing (not a bug, just an unwanted feature of
`require`).
