# Use numbered configuration files

## Context and Problem Statement

How should the config files be organized?

## Decision Drivers

* Easy to understand
* Easy to navigate
* Easy way to group config sections
* Predictable load order
* Easy config bisection

## Considered Options

* One huge `init.el` file
* Many smaller files loaded from `init.el`
* Many smaller files with a naming convention to order them

## Decision Outcome

Chosen option: "Many smaller files with a naming convention to order them", because of the reasoning below.

### One huge `init.el` file

* Good, because it's trivial to understand
* Bad, because it's hard to navigate (can be mitigated with `outline-minor-mode`)
* Bad, because there is no section grouping apart from explicit comments
* Good, because the load order is predictable
* Good, because it's easy to bisect

One personal gripe with this approach is how whenever a new
configuration section is being added, it needs to be fitted between
two existing sections.  Making the decision between which ones is
adding a significant cognitive load, discouraging config tinkering.
This is not a speculation, this is what I experienced in the past.

It happens with the smaller files too but on a much smaller scale and
the cognitive load has been much more manageable.

### Many smaller files loaded from `init.el`

For example `buffer-management.el`.

* Good, because it's an easy to understand simple loop
* Good, because it's easy to navigate, even without additional tools
* Good, because each logical section gets its own file
* Bad, because the load order is unpredictable; determined by a section name (a file name), which is meant to be arbitrary
* Bad, because it's hard to bisect, especially with the unpredictable load order

### Many smaller files with a naming convention to order them

For example `30-buffer-management.el`.

* Bad, because it's harder to understand as it uses custom logic to load the config
* Good, because it's easy to navigate, even without additional tools
* Good, because each logical section gets its own file
* Good, because the load order is explicitly specified in the file name, with the actual section name following it
* Neutral, because while direct bisecting is no longer trivial, the custom loading logic can have bisecting capabilities added

## More Information

The entire necessary logic got implemented in the `config-lib.el`
file.  The `init.el` file mostly calls the facilities defined there
with the actual configiration living in the `lisp/*` files.

Bisecting can be done by providing the optional last argument to the
`load-numbered-parts` function which limits up to which file to load
the configuration.
