# Use `straight.el` for package management

## Context and Problem Statement

Which Emacs package manager should be used?

## Decision Drivers

* Availability
* Predictability
* Installation of unpackaged software
* Disk space usage
* Ease of patching/modifying the installed packages

## Considered Options

* `package.el`
* `straight.el`

## Decision Outcome

Chosen option: `straight.el`, because its only downsides are not being
included in Emacs and the additional disk space used (can be mitigated
with `straight-vc-git-default-clone-depth` or just ignored on a larger
disk).  Its great support for version pinning (predictable installs)
and patching the installed packages far outweigh these inconveniences.

## Pros and Cons of the Options

### `package.el`

* Good, because it's built into GNU Emacs
* Bad, because there is no obvious way to pin a specific package version
* Neutral, because recently (Emacs 29) `package-vc-install` got added
* Good, because only the current package version is stored
* Bad, because there is no easy way to patch the installed package

### `straight.el`

* Bad, because it's an external dependency
* Good, because it directly supports pinning package versions
* Good, because it supports installing packages from external repositories
* Bad, because by default it stores the whole Git repository of each package
* Good, because it's directly supported to patch and modify the installed packages

## More Information

|             | built-in | version pinning | install external | low disk usage | patching |
|-------------|:--------:|:---------------:|:----------------:|:--------------:|:--------:|
| package.el  | X        | -               | X                | X              | -        |
| straight.el | -        | X               | X                | -              | X        |
