# Attach configuration to the feature providers

## Context and Problem Statement

When one package provides a hook function and the other uses it in its
mode hook, which package does this configuration belong to?

## Decision Drivers

* Configuration clarity
* The `use-package` conventions

## Considered Options

* Attach configuration to the feature consumer
* Attach configuration to the feature provider
* Detach the configuration from both

## Decision Outcome

Chosen option: "Attach configuration to the feature provider", because
it's compatible with the implicit autoload semantics of `use-package`.
All the other properties seem very much opinion-based with no
clear winner.

## Pros and Cons of the Options

### Attach configuration to the feature consumer

```elisp
(use-package yaml-mode
  :hook ((yaml-mode . outline-minor-mode)
         (yaml-mode . completion-preview-mode)))
(use-package prog-mode
  :hook ((prog-mode . outline-minor-mode)
         (prog-mode . completion-preview-mode)))
(use-package outline …)
(use-package completion-preview …)
```

* Bad, because `use-package` wrongly declares autoloads as defined by
  the hook function consumers
* Bad, because it's not immediately clear what packages consume
  a given feature
* Good, because it's clear which features enhance a given package

### Attach configuration to the feature provider

```elisp
(use-package yaml-mode …)
(use-package prog-mode …)
(use-package outline
  :hook ((yaml-mode prog-mode) . outline-minor-mode))
(use-package completion-preview
  :hook ((yaml-mode prog-mode) . completion-preview-mode))
```

* Good, because it works in tune with `use-package`, properly
  declaring the autoloads of the hook functions (though they are often
  already declared in general)
* Good, because it's clear which packages consume a given feature
* Bad, because it's not immediately clear what packages enhance
  a given package

### Detach the configuration from both

```elisp
(use-package yaml-mode …)
(use-package prog-mode …)
(use-package outline …)
(use-package completion-preview …)

(add-hook 'yaml-mode-hook #'outline-minor-mode)
(add-hook 'yaml-mode-hook #'completion-preview-mode)
(add-hook 'prog-mode-hook #'outline-minor-mode)
(add-hook 'prog-mode-hook #'completion-preview-mode)
```

* Good, because no faulty autoloads get declared
* Bad, because it's not immediately clear what packages consume
  a given feature
* Bad, because it's not immediately clear what packages enhance
  a given package
