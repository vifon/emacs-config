;;; -*- lexical-binding: t; -*-

;; Load all the unit test files from this directory.
(load-numbered-parts (file-name-directory
                      (or load-file-name
                          buffer-file-name)))
