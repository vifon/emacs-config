;;; -*- lexical-binding: t; -*-

(ert-deftest test-mail-without-alias ()
  (let ((base-addr "me@example.com"))
    (mapc (lambda (addr)
            (should (string= (vifon/mail-without-alias addr)
                             base-addr)))
          '("me+alias@example.com"
            "me@example.com"))))
