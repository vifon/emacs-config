;;; -*- lexical-binding: t; -*-

(defvar test-list)

(ert-deftest test-add-to-list-after ()
  "Test basic edge cases of `vifon/add-to-list-after'.

NB: The lists of symbols are being passed to `list' first (via
`apply' for easier quoting) so they can be safely mutated.
See the Info node `(elisp) Modifying Lists'."

  ;; Add a new "qux" element after "bar".
  (setq test-list (apply #'list '(foo bar baz)))
  (vifon/add-to-list-after 'test-list 'bar 'qux)
  (should (equal test-list
                 '(foo bar qux baz)))

  ;; "qux" already exists after "bar", don't add a second one.
  (setq test-list (apply #'list '(foo bar qux baz)))
  (vifon/add-to-list-after 'test-list 'bar 'qux)
  (should (equal test-list
                 '(foo bar qux baz)))

  ;; There is no "bar" to add "qux" after, add nothing.
  (setq test-list (apply #'list '(foo baz)))
  (vifon/add-to-list-after 'test-list 'bar 'qux)
  (should (equal test-list
                 '(foo baz)))

  ;; Add a new element only after the first match.
  (setq test-list (apply #'list '(foo bar bar baz)))
  (vifon/add-to-list-after 'test-list 'bar 'qux)
  (should (equal test-list
                 '(foo bar qux bar baz)))

  ;; Add after the last element.
  (setq test-list (apply #'list '(foo bar baz)))
  (vifon/add-to-list-after 'test-list 'baz 'qux)
  (should (equal test-list
                 '(foo bar baz qux))))

(ert-deftest test-add-to-list-after-custom-compare-fn ()
  ;; Modify a more complex data structure, like an alist.  Test case
  ;; taken from `magit-repolist-columns'.
  (setq test-list '(("Name"    25 magit-repolist-column-ident nil)
                    ("Version" 25 magit-repolist-column-version
                     ((:sort magit-repolist-version<)))
                    ("B<U"      3 magit-repolist-column-unpulled-from-upstream
                     ((:right-align t)
                      (:sort <)))
                    ("B>U"      3 magit-repolist-column-unpushed-to-upstream
                     ((:right-align t)
                      (:sort <)))
                    ("Path"    99 magit-repolist-column-path nil)))
  (vifon/add-to-list-after 'test-list
                           '("Version")
                           '("D" 1 magit-repolist-column-flag ())
                           (lambda (a b)
                             (string= (car a)
                                      (car b))))
  (should (equal test-list
                 '(("Name"    25 magit-repolist-column-ident nil)
                   ("Version" 25 magit-repolist-column-version
                    ((:sort magit-repolist-version<)))
                   ("D" 1 magit-repolist-column-flag ())
                   ("B<U"      3 magit-repolist-column-unpulled-from-upstream
                    ((:right-align t)
                     (:sort <)))
                   ("B>U"      3 magit-repolist-column-unpushed-to-upstream
                    ((:right-align t)
                     (:sort <)))
                   ("Path"    99 magit-repolist-column-path nil)))))
