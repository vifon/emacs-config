;;; -*- lexical-binding: t; -*-

(use-package puni
  :straight t
  :hook ((emacs-lisp-mode . puni-mode)
         ((sgml-mode nxml-mode tex-mode) . puni-mode)
         (eval-expression-minibuffer-setup . puni-mode))
  :bind (("M-(" . puni-wrap-round)
         ("M-s M-r" . puni-raise)
         ("M-s M-S" . puni-squeeze)

         :map puni-mode-map
         ;; Keys inspired by Paredit.
         ("M-r" . puni-raise)
         ("M-(" . puni-wrap-round)
         ("C-(" . puni-slurp-backward)
         ("C-)" . puni-slurp-forward)
         ("C-{" . puni-barf-backward)
         ("C-}" . puni-barf-forward)

         ;; Some common operations on delimiters I use.
         ("M-s M-s" . puni-splice)
         ("M-s M-S" . puni-squeeze)
         ("M-S"     . puni-split)
         ;; TODO: Is there something like hypothetical `puni-join'
         ;; that would be an inverse of `puni-split'?

         ;; Syntactic navigation.  Might get removed later if won't
         ;; end up useful.
         ("M-a" . puni-syntactic-backward-punct)
         ("M-e" . puni-syntactic-forward-punct)

         ;; Unbind `puni-beginning-of-sexp' and `puni-end-of-sexp'.
         ;; I'm used to using `backward-up-list' instead, and I do use
         ;; the original bindings these would override.
         ("C-M-a" . nil)
         ("C-M-e" . nil)))

(use-package package-lint
  :straight t
  :defer t)

(use-package highlight-defined
  :straight t
  :commands highlight-defined-mode)

(use-package macrostep
  :straight t
  :defer t)

(use-package slime
  :straight t
  :defer t
  :config (when (file-readable-p "~/quicklisp/slime-helper.el")
            (load "~/quicklisp/slime-helper")))
(use-package slime-autoloads
  :config (setq inferior-lisp-program "sbcl"))


(with-eval-after-load 'elisp-mode
  ;; Use the GNU-style tab size (8) for the core Emacs sources.
  (let* ((emacs-source-rx (rx "/emacs/" (eval emacs-version)
                              "/lisp" (? "/")
                              eos))
         (emacs-source-path (seq-find (lambda (x)
                                        (string-match-p emacs-source-rx x))
                                      load-path)))
    (if (null emacs-source-path)
        (warn "Cannot determine the Emacs source path")
      (dir-locals-set-class-variables
       'builtin-elisp
       '((emacs-lisp-mode . ((tab-width . 8)))))
      (dir-locals-set-directory-class
       emacs-source-path
       'builtin-elisp))))
