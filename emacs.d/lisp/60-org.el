;;; -*- lexical-binding: t; -*-

(use-package org
  :straight t
  :defer t
  :bind ( :map org-mouse-map
          ("S-<mouse-4>" . vifon/org-timestamp-mouse)
          ("S-<mouse-5>" . vifon/org-timestamp-mouse)
          ("C-S-<mouse-4>" . vifon/org-clock-timestamps-mouse)
          ("C-S-<mouse-5>" . vifon/org-clock-timestamps-mouse)
          ("S-<wheel-up>"   . vifon/org-timestamp-mouse)
          ("S-<wheel-down>" . vifon/org-timestamp-mouse)
          ("C-S-<wheel-up>"   . vifon/org-clock-timestamps-mouse)
          ("C-S-<wheel-down>" . vifon/org-clock-timestamps-mouse)
          :repeat-map vifon/org-archive-map
          ("$" . org-archive-subtree)
          :repeat-map vifon/org-todo-repeat-map
          ("C-t" . org-todo))
  :init (require 'org-loaddefs)
  :config (progn
            (bind-key "C-c C-1"
                      (lambda ()
                        (interactive)
                        (org-time-stamp-inactive '(16)))
                      org-mode-map)
            (bind-key [remap insert-file]
                      (lambda ()
                        (interactive)
                        (call-interactively (if (org-before-first-heading-p)
                                                #'insert-file
                                              #'vifon/org-insert-directory)))
                      org-mode-map)
            (add-to-list 'org-file-apps
                         (cons t
                               (lambda (path link)
                                 (call-process "rifle" nil 0 nil path)))
                         'append)

            (setq org-default-notes-file (expand-file-name "inbox.org" org-directory))
            (plist-put org-format-latex-options :scale 2.0)
            (setq org-adapt-indentation t)
            (setq org-time-stamp-rounding-minutes '(0 30))

            (setq org-html-doctype "html5"
                  org-html-html5-fancy t)))

(use-package org-contrib
  :straight t
  :defer t)

(use-package org-mouse
  :after org)

(use-package org-attach
  :after org
  :config (setq org-attach-use-inheritance t
                org-attach-preferred-new-method 'ask))


(use-package org-refile
  :commands org-refile
  :config (setq org-outline-path-complete-in-steps nil
                org-refile-use-outline-path 'file
                org-refile-targets '((org-agenda-files :tag . "PROJECT")
                                     (org-agenda-files :tag . "CATEGORY")
                                     (org-agenda-files :tag . "GROUP")
                                     (org-agenda-files :level . 1)
                                     (nil :tag . "PROJECT")
                                     (nil :tag . "CATEGORY")
                                     (nil :tag . "GROUP")
                                     (nil :maxlevel . 2))))

(use-package org-clock
  :commands (org-clock-goto org-clocking-p)
  :config (setq org-clock-into-drawer t
                org-clock-out-remove-zero-time-clocks t
                org-clock-rounding-minutes 30))

(use-package org-mru-clock
  :straight t
  :after org-clock
  :init (unless (bound-and-true-p org-clock-history)
          (message "Loading the org-clock history...")
          (org-mru-clock-to-history))
  :config (advice-add 'org-mru-clock-select-recent-task :after
                      #'org-back-to-heading))

(defun vifon/org-resolve-clocks-with-calc (orig &rest args)
  (cl-letf (((symbol-function 'read-number)
             (lambda (prompt &optional default)
               (string-to-number
                (calc-eval (read-string prompt
                                        nil
                                        nil default))))))
    (apply orig args)))
(advice-add 'org-resolve-clocks :around
            #'vifon/org-resolve-clocks-with-calc)

(defun org-followup ()
  (interactive)
  (let ((link (org-store-link nil)))
    (org-insert-heading-respect-content)
    (end-of-line)
    (save-excursion
      (org-return 'indent)
      (org-time-stamp-inactive '(16))
      (org-return 'indent)
      (insert "Follow-up of: " link))))

(defun vifon/org-timestamp-mouse (event)
  "Modify the org timestamps with the mouse wheel."
  (interactive "e")
  (save-excursion
    (mouse-set-point event)
    (let ((button (event-basic-type event)))
      (cond
       ((memq button '(mouse-4 wheel-up))
        (org-timestamp-up))
       ((memq button '(mouse-5 wheel-down))
        (org-timestamp-down))
       (t (error "Bad mouse button: %s" button))))))

(defun vifon/org-clock-timestamps-mouse (event)
  "Modify the org-clock timestamps with the mouse wheel."
  (interactive "e")
  (save-excursion
    (mouse-set-point event)
    (let ((button (event-basic-type event)))
      (cond
       ((memq button '(mouse-4 wheel-up))
        (org-clock-timestamps-up))
       ((memq button '(mouse-5 wheel-down))
        (org-clock-timestamps-down))
       (t (error "Bad mouse button: %s" button))))))

(defun vifon/org-insert-directory (directory &optional with-checkbox)
  "Insert DIRECTORY as a list of org-mode links.

Intended to be invoked in an empty line, at the desired
indentation level."
  (interactive (list
                (read-directory-name "Directory to insert as links: "
                                     nil (dired-dwim-target-defaults nil nil))
                current-prefix-arg))
  (let ((org-stored-links (vifon/dired-org-store-links directory))
        (prefix (concat (make-string (current-indentation) ?\s)
                        (if with-checkbox
                            "- [ ] "
                          "- "))))
    (delete-horizontal-space)
    (push-mark)
    (org-insert-all-links nil prefix)
    (delete-blank-lines)))

(defun vifon/dired-org-store-links (directory)
  "Return a list of all files in `directory' as an org-mode link."
  (require 'org)
  (require 'ol)
  (require 'dash)
  (with-current-buffer (find-file-noselect directory)
    (save-excursion
      (goto-char (point-min))
      (let ((links))
        (while (not (eobp))
          (when-let ((file (dired-get-filename nil t)))
            (unless (member (file-name-nondirectory file)
                            '("." ".."))
              (push (list (concat "file:" file)
                          (file-name-nondirectory file))
                    links)))
          (forward-line 1))
        (nreverse links)))))

(use-package ol-notmuch
  :straight t
  :after (:any org notmuch)
  :if (executable-find "notmuch"))

(use-package ol-tmsu
  :straight t
  :after (:any org tmsu-dired)
  :if (executable-find "tmsu"))

(use-package org-protocol :after org)
(use-package org-inlinetask :after org)

(setq org-hide-leading-stars nil)
(setq org-special-ctrl-a/e t)
(setq org-use-speed-commands t)
(setq org-use-fast-tag-selection t)
(setq org-ellipsis "[…]")
(setq org-indent-mode-turns-off-org-adapt-indentation nil)

;; A quick and dirty improvement so that the links stored from dired
;; have the file basename as their description, as opposed to not
;; having a description at all.
(with-eval-after-load 'ol
  (defun vifon/org-dired-store-link+ ()
    "Taken from `org-store-link' and improved."
    (when (derived-mode-p 'dired-mode)
      (let ((file (dired-get-filename nil t)))
        (setq file (if file
                       (abbreviate-file-name
                        (expand-file-name (dired-get-filename nil t)))
                     ;; otherwise, no file so use current directory.
                     (dired-current-directory)))
        (org-link-store-props :type "file"
                              :link file
                              :description (file-name-nondirectory file)))))
  (org-link-set-parameters
   "file"
   :store #'vifon/org-dired-store-link+))


;; https://archive.is/W2alg
(font-lock-add-keywords 'org-mode
                        '(("^ +\\(*\\) "
                           (0 (prog1 nil
                                (compose-region (match-beginning 1)
                                                (match-end 1)
                                                "•"))))))

(setq org-agenda-skip-scheduled-if-done t
      org-agenda-skip-deadline-if-done t
      org-agenda-skip-timestamp-if-done nil)
(setq org-agenda-log-mode-items '(closed))
(setq org-log-done 'time
      org-log-into-drawer t)
(setq org-icalendar-combined-agenda-file "~/org/org.ics"
      org-icalendar-use-deadline '(event-if-todo)
      org-icalendar-use-scheduled '(event-if-todo)
      org-icalendar-timezone "Europe/Warsaw"
      org-export-with-tasks 'todo)
(setq org-export-with-toc nil
      org-html-validation-link nil)
(setq org-blank-before-new-entry '((heading . nil)
                                   (plain-list-item . auto)))
(setq org-tags-exclude-from-inheritance '("PROJECT" "ATTACH"))
(setq org-todo-keyword-faces '(("NEXT" . "Tomato")))
(setq org-enforce-todo-dependencies t)

(setq org-cycle-open-archived-trees nil)
(setq org-archive-default-command 'org-archive-to-archive-sibling)
(setq org-archive-location ".archive/%s_archive::")
(setq org-deadline-warning-days 10)
(setq org-catch-invisible-edits 'smart)
(setq org-id-link-to-org-use-id 'use-existing)
(setq org-return-follows-link t)

(setq org-image-actual-width nil)

(setq org-confirm-elisp-link-function #'y-or-n-p)

(setq org-agenda-files (if (file-exists-p "~/org/.agenda-files")
                           "~/org/.agenda-files"
                         '("~/org/gtd.org")))

(bind-key "C-c a" #'org-agenda)
(bind-key "C-c S" #'org-store-link)


(defun vifon/truncate-org-mode-line ()
  (let* ((heading-text (nth 4 (org-heading-components)))
         (text-without-links
          (if (string-prefix-p "[[" heading-text)
              (replace-regexp-in-string ".*\\]\\[\\(.*\\)\\]"
                                        "\\1" heading-text)
            heading-text))
         (max-length 10))
    (replace-regexp-in-string (concat "\\(.\\{"
                                      (number-to-string max-length)
                                      "\\}[^[:space:]]*\\).*")
                              "\\1…" text-without-links)))
(setq org-clock-heading-function #'vifon/truncate-org-mode-line)

(with-eval-after-load 'org-keys
  (setf (alist-get "z" org-speed-commands
                   nil nil #'equal)
        #'org-kill-note-or-show-branches))

(setq org-stuck-projects
      '("PROJECT/-MAYBE-DONE-ABORTED"
        ("NEXT")
        ("RECURRING")
        "\\<IGNORE\\>"))

(use-package ob
  :defer t
  :after org
  :config (progn
            (setq org-confirm-babel-evaluate nil
                  org-export-use-babel nil)
            (org-babel-do-load-languages
             'org-babel-load-languages
             '((shell . t)
               (awk . t)
               (makefile . t)

               (ditaa . t)
               (plantuml . t)
               (dot . t)
               (gnuplot . t)
               (octave . t)

               (sqlite . t)

               (haskell . t)

               (C . t)

               (python . t)
               (perl . t)

               (java . t)

               (js . t)))
            (setq org-babel-C-compiler "gcc -std=c99"
                  org-babel-C++-compiler "g++ -std=c++14"
                  org-babel-python-command "python3"
                  org-babel-perl-preface "use 5.010;")))

(use-package org-crypt
  :after org
  :config (progn
            (org-crypt-use-before-save-magic)
            (add-to-list 'org-tags-exclude-from-inheritance "crypt")
            (setq org-crypt-key "890029F6")))

(use-package org-habit
  :after org-agenda
  :config (setq org-habit-show-habits-only-for-today nil
                org-habit-show-all-today nil
                org-agenda-show-future-repeats 'next
                org-habit-show-habits nil))


(use-package org-duration
  :after org
  :config (setq org-duration-units `(("min" . 1)
                                     ("h" . 60)
                                     ("d" . ,(* 60 8))
                                     ("w" . ,(* 60 8 5))
                                     ("m" . ,(* 60 8 5 4))
                                     ("y" . ,(* 60 8 5 4 11)))))

(use-package steam
  :straight t
  :defer t
  :config (setq steam-username "vifon"))
