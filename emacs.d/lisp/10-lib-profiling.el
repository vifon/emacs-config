;;; -*- lexical-binding: t; -*-

(defmacro vifon/with-profiler (mode &rest body)
  "Profile the BODY execution and call `profiler-report'.

See `profiler-start' for MODE."
  (declare (indent 1))
  `(unwind-protect
       (progn
         (profiler-start ,mode)
         ,@body)
     (profiler-stop)
     (profiler-report)))
(defalias 'with-profiler 'vifon/with-profiler)

(defun vifon/profiler--advice (orig &rest args)
  (unwind-protect
      (progn
        (profiler-start 'cpu)
        (apply orig args))
    (profiler-stop)
    (profiler-report)))

(defun vifon/profiler-instrument-function (func)
  "Attach a profiler to each FUNC call.

The profiler is restarted for each call and `profiler-report' is
called after each one."
  (interactive "aFunction to instrument: ")
  (advice-add func :around #'vifon/profiler--advice))
(defalias 'profiler-instrument-function 'vifon/profiler-instrument-function)
