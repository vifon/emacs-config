;;; -*- lexical-binding: t; -*-

(use-package beacon
  :straight t
  :diminish beacon-mode
  :config (beacon-mode 1))

(use-package goggles
  :straight t
  :hook ((prog-mode text-mode) . goggles-mode)
  :diminish goggles-mode
  :config (setq-default goggles-pulse nil))

(use-package symbol-overlay
  :straight t
  :bind (("M-p" . symbol-overlay-jump-prev)
         ("M-n" . symbol-overlay-jump-next)
         ("C-;" . symbol-overlay-put)
         ("C-M-;" . symbol-overlay-remove-all)
         :map symbol-overlay-map
         ("b" . symbol-overlay-switch-backward)
         ("f" . symbol-overlay-switch-forward)))

(use-package highlight-indent-guides
  :straight t
  :commands highlight-indent-guides-mode
  :config (setq highlight-indent-guides-method 'column)
  :init (define-globalized-minor-mode global-highlight-indent-guides-mode highlight-indent-guides-mode
          (lambda ()
            (when (derived-mode-p 'prog-mode)
              (highlight-indent-guides-mode 1)))))

(use-package color-identifiers-mode
  :straight t
  :defer t)

(use-package rainbow-mode
  :straight t
  :hook (css-mode . rainbow-mode))

(use-package treesit-auto
  :if (version<= "29" emacs-version)
  :straight t
  :custom (treesit-auto-install 'prompt)
  :config (progn
            (setq treesit-auto-langs '(python go rust))
            (treesit-auto-add-to-auto-mode-alist 'all)
            (global-treesit-auto-mode 1)))
