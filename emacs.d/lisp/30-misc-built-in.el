;;; -*- lexical-binding: t; -*-

(use-package paren
  :init (show-paren-mode 1)
  :config (setq show-paren-delay 0
                show-paren-when-point-inside-paren t
                show-paren-when-point-in-periphery t
                show-paren-context-when-offscreen 'overlay))

(use-package repeat
  ;; Added in Emacs 28.
  :if (fboundp 'repeat-mode)
  :init (repeat-mode 1))

(use-package simple
  :bind ( :repeat-map vifon/transpose-lines-repeat-map
          ("C-t" . transpose-lines)))

(use-package newcomment
  :bind ( :repeat-map vifon/comment-line-repeat-map
          ("C-;" . comment-line)))


(defalias 'vifon/comment-line-after-duplicate 'comment-line
  "Alias for `comment-line' for use in `vifon/duplicate-line-repeat-map'.")

(use-package misc
  ;; Added in Emacs 29.
  :if (fboundp 'duplicate-dwim)
  :bind (("C-x j" . duplicate-dwim)
         :repeat-map vifon/duplicate-line-repeat-map
         ("j" . duplicate-dwim)
         (";" . vifon/comment-line-after-duplicate)))

(use-package isearch
  :config (setq isearch-lazy-count t))

(use-package image-mode
  :bind ( :map image-mode-map
          ("k" . kill-buffer)
          ("K" . kill-buffer-and-window)))

(use-package display-line-numbers
  :hook ((yaml-mode prog-mode) . display-line-numbers-mode)
  :config (setq-default display-line-numbers-widen t))

(use-package display-fill-column-indicator
  :hook (git-commit-mode . display-fill-column-indicator-mode))

(use-package cua-base
  :bind ("C-S-SPC" . cualess-global-mark)
  :init (progn
          ;; Disabling `cua-global-mark-keep-visible' improves
          ;; performance when typing characters with global-mark
          ;; enabled.
          (setq cua-global-mark-keep-visible nil)

          ;; Do not enable more cua keys than necessary.
          (setq cua-enable-cua-keys nil)

          ;; Using `cua-selection-mode' alone is not enough. It still
          ;; binds C-RET and overrides this keybinding for instance in
          ;; org-mode. I'd rather keep using my hack.
          (defun cualess-global-mark ()
            (interactive)
            (cua-selection-mode 1)
            (call-interactively #'cua-toggle-global-mark))
          (advice-add 'cua--deactivate-global-mark :after
                      (lambda (ret-value)
                        (cua-mode 0)
                        ret-value))))
