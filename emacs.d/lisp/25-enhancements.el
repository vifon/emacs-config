;;; -*- lexical-binding: t; -*-

;; Enhancements of the built-in Emacs functionality.

(defun vifon/indent-relative-dwim (&optional first-only unindented-ok)
  "Like `indent-relative' but disable `indent-tabs-mode' when aligning."
  (interactive "P")
  (let* ((at-indent-p (save-excursion
                        (skip-chars-backward " \t")
                        (eq (point) (point-at-bol))))
         (indent-tabs-mode (if at-indent-p
                               indent-tabs-mode
                             nil)))
    (indent-relative first-only unindented-ok)))
(bind-key [remap indent-relative] #'vifon/indent-relative-dwim)

(when (functionp 'sqlite-available-p)
  (defun vifon/sqlite-mode-reopen-file ()
    "Reopen the buffer as an SQLite database."
    (interactive)
    (let ((f buffer-file-name))
      (kill-buffer)
      (sqlite-mode-open-file f)))
  (defun vifon/sqlite-mode-reopen-file-maybe ()
    "Ask whether to call `vifon/sqlite-mode-reopen-file'."
    (interactive)
    (when (y-or-n-p "Reopen using `sqlite-mode-open-file'?")
      (vifon/sqlite-mode-reopen-file)))
  (add-to-list 'magic-fallback-mode-alist
               '("SQLite format 3" . vifon/sqlite-mode-reopen-file-maybe)))


(defun vifon/macroexpand-advice--use-package-minimally (orig expression)
  "Enable `use-package-expand-minimally' when expanding `use-package' forms.

This greatly reduces the noise created by the error handling that
`use-package' does otherwise.  Do not use this advice when specifically
debugging this error handling!"
  (if (eq (car expression) 'use-package)
      (let ((use-package-expand-minimally t))
        (funcall orig expression))
    (funcall orig expression)))

(advice-add 'pp-macroexpand-expression :around
            #'vifon/macroexpand-advice--use-package-minimally)
