;;; -*- lexical-binding: t; -*-

(use-package ibuffer
  :bind ("C-x C-b" . ibuffer-jump)
  :config (progn
            (add-hook 'ibuffer-mode-hook
                      (lambda ()
                        (setq ibuffer-sorting-mode 'filename/process)))
            (setq ibuffer-show-empty-filter-groups nil)
            (setq ibuffer-expert t)
            (setq ibuffer-formats
                  `((mark modified read-only locked
                          " "
                          (name 32 32 :left :elide)
                          " "
                          (size 9 -1 :right)
                          " "
                          (mode 16 64 :left :elide)
                          " " filename-and-process)
                    (mark " "
                          (name 32 -1)
                          " " filename)))))

(use-package ibuffer-vc
  :straight t
  :bind ( :map ibuffer-mode-map
          ("/ V" . ibuffer-vc-set-filter-groups-by-vc-root)))

(use-package ibuffer-tramp
  :straight t
  :bind ( :map ibuffer-mode-map
          ("/ T" . ibuffer-tramp-set-filter-groups-by-tramp-connection)))

(use-package tab-bar
  :defer t
  :config (progn
            ;; `tab-bar-format-tabs' replaced with
            ;; `tab-bar-format-tabs-groups' to enable the tab
            ;; groups functionality.
            (setq tab-bar-format '(tab-bar-format-history
                                   tab-bar-format-tabs-groups
                                   tab-bar-separator
                                   tab-bar-format-add-tab))
            (setq tab-bar-show 1
                  tab-bar-tab-hints t
                  tab-bar-select-tab-modifiers '(meta)
                  tab-bar-tab-name-function #'tab-bar-tab-name-current-with-count)))

(use-package window
  :bind (("C-x <down>" . previous-buffer)
         ("C-x <up>"   . next-buffer)
         :repeat-map vifon/buffer-nextprev-repeat-map
         ("<up>"    . next-buffer)
         ("<right>" . next-buffer)
         ("<down>"  . previous-buffer)
         ("<left>"  . previous-buffer)))

(use-package recentf
  :init (recentf-mode 1)
  :config (setq recentf-exclude '("\\`/media/"
                                  "\\`/run/media/")
                recentf-max-saved-items 1000))

(use-package midnight
  :config (midnight-mode 1))

(use-package winner
  :bind ("C-c z" . winner-undo)
  :init (winner-mode 1))
