;;; -*- lexical-binding: t; -*-

(use-package vertico
  :straight (vertico :files (:defaults "extensions/*"))
  :bind (("C-x M-r" . vertico-repeat)
         :map vertico-map
         ("C-l" . vertico-directory-delete-word)
         ("C-c C-g" . vertico-multiform-grid)
         ("M-q"     . vertico-multiform-flat))
  :init (vertico-mode 1)
  :config (progn
            (add-hook 'minibuffer-setup-hook #'vertico-repeat-save)
            (vertico-mouse-mode 1)
            (vertico-multiform-mode 1)
            (setq vertico-multiform-categories '((consult-grep buffer))
                  vertico-multiform-commands '((tmm-menubar flat)
                                               (tmm-shortcut flat)))

            ;; Needed with `read-file-name-completion-ignore-case'.
            ;; See these links:
            ;; - https://github.com/minad/vertico/issues/341
            ;; - https://debbugs.gnu.org/cgi/bugreport.cgi?bug=60264
            ;;
            ;; Regardless of it fixing an actual bug, I prefer
            ;; this behavior.
            (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)))

(use-package orderless
  :straight t
  :after vertico
  :config (progn
            (setq orderless-matching-styles '(orderless-regexp
                                              orderless-initialism
                                              orderless-prefixes)
                  orderless-component-separator #'orderless-escapable-split-on-space)

            ;; Use the built-in `partial-completion' style to complete
            ;; file inputs such as "/e/ni/co.nix" into
            ;; "/etc/nixos/configuration.nix".
            (setq completion-category-defaults nil
                  completion-category-overrides '((file (styles partial-completion))))

            ;; Make the stock file completion styles ("basic" and
            ;; "partial-completion") case insensitive, it fits better
            ;; with the behavior provided by orderless.  See the
            ;; `orderless-smart-case' documentation for how it
            ;; interacts with orderless itself (spoiler: in this setup
            ;; it doesn't).
            (setq read-file-name-completion-ignore-case t
                  completion-ignore-case t)

            (setq completion-styles '(orderless basic))

            (defun vifon/call-without-orderless-dispatchers (orig &rest args)
              "Use with `advice-add' (`:around') to ignore the dispatchers."
              (let ((orderless-style-dispatchers nil))
                (apply orig args)))))

(use-package embark
  :straight t
  :bind (("C-c o" . embark-dwim)
         ("C-."   . embark-act)
         :map minibuffer-local-map
         ("M-o"   . embark-act)
         :map embark-command-map
         ;; Unbind the dangerous `global-set-key' and `local-set-key'
         ;; actions.  It's far too easy to accidentally bind over some
         ;; `self-insert-command' binding or even over
         ;; \\[keyboard-quit].
         ("g" . nil)
         ("l" . nil)
         :map embark-collect-mode-map
         ("m" . vifon/embark-select-and-forward))
  :config (progn
            (setq embark-mixed-indicator-delay 2)

            ;; Preserve the original values before mutating them with
            ;; `cl-pushnew' below.
            (setq embark-post-action-hooks
                  (copy-alist embark-post-action-hooks))
            (setq embark-target-injection-hooks
                  (copy-alist embark-target-injection-hooks))

            ;; Make the eval action editable.  Evaluating code
            ;; in-place is simple enough without Embark, if I invoke
            ;; it with Embark, I almost definitely want to edit the
            ;; expression beforehand.  And even if not, I can
            ;; just confirm.
            (cl-pushnew 'embark--allow-edit
                        (alist-get 'pp-eval-expression embark-target-injection-hooks))

            ;; Reload the project list after using
            ;; C-u `embark-act' with `project-forget-project'.
            (cl-pushnew 'embark--restart
                        (alist-get 'project-forget-project embark-post-action-hooks))

            (defun embark-act-with-eval (expression)
              "Evaluate EXPRESSION and call `embark-act' on the result."
              (interactive "sExpression: ")
              (with-temp-buffer
                (let ((expr-value (eval (read expression))))
                  (insert (if (stringp expr-value)
                              expr-value
                            (format "%S" expr-value))))
                (embark-act)))

            (dolist (keymap (list embark-variable-map embark-expression-map))
              (define-key keymap (kbd "v") #'embark-act-with-eval))

            ;; Source: https://github.com/oantolin/embark/wiki/Additional-Actions#attaching-file-to-an-email-message
            (autoload 'gnus-dired-attach "gnus-dired" nil t)
            (defun embark-attach-file (file)
              "Attach FILE to an email message."
              (interactive "fAttach: ")
              (cl-letf (((symbol-function 'y-or-n-p) #'always))
                (gnus-dired-attach (list file))))
            (bind-key "a" #'embark-attach-file embark-file-map)

            (defun vifon/embark-select-and-forward ()
              (interactive)
              (embark-select)
              (forward-button 1))))

(use-package embark-consult
  :straight t
  :after (embark consult))

(use-package marginalia
  :straight t
  :after vertico
  :demand t                     ; `:demand' applies to `:bind' but not
                                ; `:after.'  We want to eagerly load
                                ; marginalia once vertico is loaded.
  :bind ( :map minibuffer-local-map
          ("C-o" . marginalia-cycle))
  :config (marginalia-mode 1))

(use-package consult
  :straight t
  :bind ( :map consult-mode-map
          ;; M-s …
          ("M-s u" . consult-focus-lines)
          ("M-s k" . consult-keep-lines)
          ("M-s e" . consult-isearch-history)
          ("M-s d" . consult-fd)
          ;; M-g …
          ("M-g g" . consult-line)
          ("M-g o" . consult-outline)
          ("M-g i" . consult-imenu)
          ("M-g I" . consult-info)
          ("M-g r" . consult-ripgrep)
          ("M-g m" . consult-mark)
          ("M-g M" . consult-global-mark)
          ;; Misc.
          ("C-x C-r" . consult-recent-file)
          ;; Remaps
          ([remap switch-to-buffer]              . consult-buffer)
          ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
          ([remap switch-to-buffer-other-frame]  . consult-buffer-other-frame)
          ([remap project-switch-to-buffer]      . consult-project-buffer)
          ([remap yank-pop]                      . consult-yank-pop)
          ([remap goto-line]                     . consult-goto-line)
          ([remap bookmark-jump]                 . consult-bookmark)
          ([remap repeat-complex-command]        . consult-complex-command)
          ;; Remaps for `Info-mode'.
          ([remap Info-search] . consult-info)

          :map isearch-mode-map
          ("TAB" . consult-line))
  :init (progn
          (defvar consult-mode-map (make-sparse-keymap))
          (define-minor-mode consult-mode
            "Provide the `consult' commands in a single keymap."
            :global t
            (if consult-mode
                (define-key minibuffer-local-map
                            [remap previous-matching-history-element]
                            #'consult-history)
              (define-key minibuffer-local-map
                          [remap previous-matching-history-element]
                          nil)))
          (consult-mode 1))
  :config (progn
            (consult-customize
             consult-ripgrep consult-git-grep consult-grep
             consult-bookmark consult-recent-file consult-xref
             consult--source-bookmark consult--source-file-register
             consult--source-recent-file consult--source-project-recent-file
             consult-buffer
             :preview-key "M-.")

            (defun vifon/orderless-fix-consult-tofu (pattern index total)
              "Ignore the last character which is hidden and used only internally."
              (when (string-suffix-p "$" pattern)
                `(orderless-regexp . ,(concat (substring pattern 0 -1)
                                              "[\x200000-\x300000]*$"))))

            (dolist (command '(consult-buffer consult-line))
              (advice-add command :around
                          (lambda (orig &rest args)
                            (let ((orderless-style-dispatchers (cons #'vifon/orderless-fix-consult-tofu
                                                                     orderless-style-dispatchers)))
                              (apply orig args)))))

            ;; Disable consult-buffer project-related capabilities as
            ;; they are very slow in TRAMP.
            (setq consult-buffer-sources
                  (delq 'consult--source-project-buffer
                        (delq 'consult--source-project-file consult-buffer-sources)))

            (setq consult--source-hidden-buffer
                  (plist-put consult--source-hidden-buffer :narrow ?h))

            (defvar vifon/consult--source-disassociated-file-buffer
              `( :name     "Disassociated File"
                 :narrow   ?e
                 :category buffer
                 :state    ,#'consult--buffer-state
                 :items
                 ,(lambda ()
                    (consult--buffer-query :sort 'visibility
                                           :as #'buffer-name
                                           :predicate
                                           (lambda (buf)
                                             (let ((file (vifon/buffer-file-or-directory-name buf)))
                                               (and file (not (file-exists-p file)))))))
                 "Disassociated buffer candidate source for `consult-buffer'.

Inspired by: `ibuffer-mark-dissociated-buffers'."))
            (defun vifon/consult-disassociated-buffers ()
              "Like `consult-buffer' but only for disassociated buffers."
              (interactive)
              (consult-buffer '(vifon/consult--source-disassociated-file-buffer)))


            (defvar vifon/consult--source-remote-file-buffer
              `( :name     "Remote File"
                 :narrow   ?r
                 :hidden   t
                 :category buffer
                 :state    ,#'consult--buffer-state
                 :items
                 ,(lambda ()
                    (consult--buffer-query :sort 'visibility
                                           :as #'buffer-name
                                           :predicate
                                           (lambda (buf)
                                             (let ((file (vifon/buffer-file-or-directory-name buf)))
                                               (and file (file-remote-p file))))))
                 "Remote file buffer candidate source for `consult-buffer'."))
            (add-to-list 'consult-buffer-sources
                         'vifon/consult--source-remote-file-buffer
                         'append)

            ;; Use Consult to select xref locations with preview.
            (setq xref-show-xrefs-function #'consult-xref
                  xref-show-definitions-function #'consult-xref)

            (add-to-list 'consult-bookmark-narrow
                         '(?t "TMSU" tmsu-dired-bookmark-open))))

(use-package corfu
  :straight (corfu :files (:defaults "extensions/*"))
  :init (global-corfu-mode 1)
  :config (progn
            (corfu-popupinfo-mode 1)
            (corfu-echo-mode 1)
            (setq corfu-popupinfo-delay '(nil . 0.3)
                  corfu-echo-delay 0.3)))

(use-package completion-preview
  ;; Added in Emacs 30.
  :if (fboundp 'completion-preview-mode)
  :hook ((yaml-mode prog-mode) . completion-preview-mode)
  :bind ( :map completion-preview-active-mode-map
          ("RET" . completion-preview-insert)
          ("TAB" . completion-preview-complete)
          ("M-n" . completion-preview-next-candidate)
          ("M-p" . completion-preview-prev-candidate)))


(defun vifon/dabbrev-capf-enable (&optional check-other-buffers)
  "Set up dabbrev-based CAPF completion in the current buffer.

Unless CHECK-OTHER-BUFFERS (\\[universal-argument]) is non-nil, set
`cape-dabbrev-check-other-buffers' to nil.  Otherwise it's
left unmodified."
  (interactive "P")
  (unless check-other-buffers
    (setq-local cape-dabbrev-check-other-buffers nil))
  (add-hook 'completion-at-point-functions #'cape-dabbrev nil t))

(use-package cape
  :straight t
  :commands cape-dabbrev
  :hook (yaml-mode . vifon/dabbrev-capf-enable))


;; https://archive.is/Gj6Fu
(autoload 'ffap-file-at-point "ffap")
(defun complete-path-at-point+ ()
  (let ((fn (ffap-file-at-point))
        (fap (thing-at-point 'filename)))
    (when (and (or fn (equal "/" fap))
               (save-excursion
                 (search-backward fap (line-beginning-position) t)))
      (list (match-beginning 0)
            (match-end 0)
            #'completion-file-name-table :exclusive 'no))))
(add-hook 'completion-at-point-functions
          #'complete-path-at-point+
          'append)

;; Add prompt indicator to `completing-read-multiple'.
;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
;;
;; Taken from the Vertico docs.
(defun crm-indicator (args)
  (cons (format "[CRM%s] %s"
                (replace-regexp-in-string
                 "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                 crm-separator)
                (car args))
        (cdr args)))
(advice-add 'completing-read-multiple :filter-args #'crm-indicator)

(setq enable-recursive-minibuffers t)
(minibuffer-depth-indicate-mode 1)

;; Use the completing-read UI for the M-tab completion unless
;; overridden (for example by `corfu').
(add-function :around completion-in-region-function
              (lambda (orig &rest args)
                (apply (if vertico-mode
                           #'consult-completion-in-region
                         orig)
                       args)))
