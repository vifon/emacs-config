;;; -*- lexical-binding: t; -*-

(defvar vifon/eshell-visual-commands-original nil
  "The original value of `eshell-visual-commands' to restore later.

Initialized by `vifon/eat-eshell-mode-toggle-visual-commands'.")

(defun vifon/eat-eshell-mode-toggle-visual-commands ()
  "A hook for `eat-eshell-mode-hook'.

Sets `eshell-visual-commands' to nil while the mode is active."
  ;; Store the original value to restore when disabling the mode.
  (unless vifon/eshell-visual-commands-original
    (require 'em-term)
    (setq vifon/eshell-visual-commands-original
          eshell-visual-commands))
  (if eat-eshell-mode
      ;; With `eat-eshell-mode' enabled this list is better off
      ;; empty, so that `eat' can handle every command directly.
      (setq eshell-visual-commands nil)
    (setq eshell-visual-commands
          vifon/eshell-visual-commands-original)))

(defun vifon/eat-from-dired ()
  "Call `eat' with a dired-aware `default-directory'."
  (interactive)
  (vifon/with-dired-directory
    (call-interactively #'eat)))

(use-package eat
  :straight (eat :files (:defaults "terminfo"
                                   "integration")
                 :fork ( :host codeberg
                         :repo "vifon/emacs-eat"
                         :branch "fish-integration"))
  :bind (("C-x T" . eat)
         ("M-O"   . eat)
         ("C-x 4 T" . eat-other-window)
         :map project-prefix-map
         ("t"   . eat-project)
         ("4 t" . eat-project-other-window)
         :repeat-map vifon/eat-prompt-nav-repeat-map
         ("C-p" . eat-previous-shell-prompt)
         ("C-n" . eat-next-shell-prompt))
  :init (with-eval-after-load 'dired
          (bind-key [remap eat] #'vifon/eat-from-dired dired-mode-map))
  :config (setq eat-tramp-shells '(("docker" . "/bin/sh")
                                   ("ssh" . "/bin/bash")
                                   ("sshx" . "/bin/bash")))
  :hook (eat-eshell-mode . vifon/eat-eshell-mode-toggle-visual-commands))
