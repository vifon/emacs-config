;;; -*- lexical-binding: t; -*-

(use-package auctex
  :straight t
  :defer t
  :init (progn
          (setq preview-scale-function 2.0)
          (defun my-auctex-build-pdf ()
            (interactive)
            (TeX-command "LaTeX" 'TeX-master-file))))

(use-package transpose-frame
  :straight t
  :bind (("C-x 4 t" . transpose-frame)
         ("C-x 4 i" . flop-frame)
         ("C-x 4 I" . flip-frame)))

(use-package goto-last-change
  :straight t
  :bind ("C-x C-\\" . goto-last-change))

(use-package presentation
  :straight t
  :commands presentation-mode)

(use-package expand-region
  :straight t
  :bind (("C-=" . er/expand-region)
         ("M-S-<SPC>" . er/expand-region)))

(use-package undo-propose
  :straight t
  :commands undo-propose)

(use-package avy
  :straight t
  :bind ("C-c j" . avy-goto-char-timer))

(use-package mosey
  :straight t
  :bind ([remap move-beginning-of-line] . mosey-backward-bounce))

(use-package terraform-mode
  :straight t
  :defer t)

(use-package yaml-mode
  :straight t
  :defer t)

(use-package just-mode :straight t :defer t)

(use-package minimap
  :straight t
  :config (setq minimap-window-location 'right))

(use-package vlf
  :straight t
  :init (require 'vlf-setup))

(use-package which-key
  :straight t
  :diminish which-key-mode
  :init (which-key-mode 1))

(use-package writeroom-mode :straight t :defer t)
(use-package olivetti :straight t :defer t)
(use-package adaptive-wrap
  :straight t
  :hook ((olivetti-mode writeroom-mode) . adaptive-wrap-prefix-mode))

(use-package aggressive-indent :straight t :defer t)
(use-package dockerfile-mode :straight t :defer t)
(use-package dpaste :straight t :defer t)
(use-package impatient-mode :straight t :defer t)
(use-package web-beautify :straight t :defer t)
(use-package orgalist :straight t :defer t)
