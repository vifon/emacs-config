;;; -*- lexical-binding: t; -*-

(defgroup vifon/terminal nil
  "External terminal launcher utils.")

(defcustom vifon/terminal-program "alacritty"
  "The terminal program to run with `vifon/terminal-program'.

Just a program name, no arguments are supported (will get passed
to `call-process' with no pre-processing)."
  :type '(choice
          (string :tag "Command")
          (const :tag "Off" nil)))

(defcustom vifon/terminal-tmux-arguments '("split-window" "-h")
  "Arguments to pass to tmux in `vifon/terminal-open'."
  :type '(choice
          (repeat :tag "tmux arguments" string)
          (const :tag "Off" nil)))

(defcustom vifon/terminal-fallback-command #'vifon/eat-from-dired
  "The command to call in `vifon/terminal-program' as a fallback.

Used in case neither a window system nor tmux are available."
  :type 'function)

(defun vifon/terminal-open ()
  "Launch an external terminal emulator in `default-directory'.

Set `vifon/terminal-program' to override the exact program.

If there is no window system available but we're running in tmux,
use a tmux split instead by calling tmux with
`vifon/terminal-tmux-arguments'.

Otherwise call `vifon/terminal-fallback-command'.

Set either of these variables to nil to immediately fall back to
the next option."
  (interactive)
  (vifon/with-dired-directory
    (cond
     ((and vifon/terminal-program (window-system))
      (call-process vifon/terminal-program nil 0 nil))
     ((and vifon/terminal-tmux-arguments (getenv "TMUX"))
      (call-process "tmux" nil 0 nil
                    "split-window" "-h"))
     (t (funcall vifon/terminal-fallback-command)))))

(bind-key "C-c x" #'vifon/terminal-open)
(bind-key "M-o" #'vifon/terminal-open)
