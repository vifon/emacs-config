;;; -*- lexical-binding: t; -*-

(use-package ledger-mode
  :straight t
  :defer t
  :bind ( :map ledger-mode-map
          ("C-M-i" . completion-at-point))
  :config (progn
            (setq ledger-clear-whole-transactions t
                  ledger-highlight-xact-under-point nil
                  ledger-default-date-format ledger-iso-date-format
                  ledger-reconcile-default-commodity "PLN"
                  ledger-post-amount-alignment-at :end)
            (defun vifon/delete-blank-lines (&rest ignored)
              "Same as `delete-blank-lines' but accept (and
ignore) any passed arguments to work as an advice."
              (let ((point (point)))
                (goto-char (point-max))
                (delete-blank-lines)
                (goto-char point)))
            (advice-add 'ledger-add-transaction :after
                        #'vifon/delete-blank-lines)
            (advice-add 'ledger-fully-complete-xact :after
                        #'vifon/delete-blank-lines)))

(use-package circe
  :straight t
  :defer t
  :bind ( :map lui-mode-map
          ("C-c C-w" . lui-track-bar-move)
          :map circe-mode-map
          ("C-c C-q". bury-buffer))
  :init (defun vifon/circe-setup ()
          (interactive)
          (let ((servers (eval (read (shell-command-to-string
                                      "pass emacs/circe/servers.el")))))
            (setq circe-network-options servers)))
  :config (progn
            (setq circe-server-buffer-name "Circe:{network}"
                  circe-reduce-lurker-spam t
                  circe-use-cycle-completion t)

            (setq circe-format-server-topic
                  (replace-regexp-in-string "{new-topic}"
                                            "{topic-diff}"
                                            circe-format-server-topic))

            (enable-circe-color-nicks)
            (setq circe-color-nicks-everywhere t)

            (enable-lui-track-bar)

            (bind-key "C-c C-o"
                      (lambda ()
                        (interactive)
                        (ffap-next-url t))
                      lui-mode-map)))

(use-package nov
  :straight t
  :mode ("\\.epub\\'" . nov-mode))

(use-package chronos
  :straight t
  :defer t)

(use-package restclient
  :straight t
  :defer t)
