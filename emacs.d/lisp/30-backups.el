;;; -*- lexical-binding: t; -*-

(setq backup-directory-alist
      `(("\\`/home/" . ,(no-littering-expand-var-file-name "backup/"))))
(setq backup-by-copying t)

(defcustom vifon/backup-exclude-prefixes
  (list "/dev/shm/")
  "A list of file path prefixes to never backup.

See `vifon/backup-enable-predicate--advice'."
  :group 'backup
  :type '(list string))

(defun vifon/backup-enable-predicate--advice (orig name)
  "Augment `backup-enable-predicate' with custom exclusion rules."
  (let ((accepted (funcall orig name)))
    (and accepted
         (not (string-match-p (concat "\\`" (regexp-opt vifon/backup-exclude-prefixes))
                              name)))))

(add-function :around backup-enable-predicate
              #'vifon/backup-enable-predicate--advice)
