;;; -*- lexical-binding: t; -*-

(defcustom vifon/scratch-dir-root "~/scratch.d"
  "The directory containing new scratch directories."
  :type 'directory)

(defun vifon/scratch-dir-path (name)
  (file-name-as-directory
   (expand-file-name
    (concat "scratch-"
            (format-time-string "%Y-%m-%d_%s")
            (when (and name (not (string= name "")))
              (concat "--" name)))
    vifon/scratch-dir-root)))

(define-obsolete-function-alias
  'scratch-dir 'vifon/make-scratch-dir "2021-11-04")

(defun vifon/make-scratch-dir (&optional name git)
  "Create an ad-hoc working directory at NAME and open it in dired.

Prefix argument GIT initializes it as a Git repository."
  (interactive "MScratch directory name: \nP")
  (let ((scratch (expand-file-name (vifon/scratch-dir-path name))))
    (make-directory scratch t)
    (when (file-symlink-p "~/scratch")
      (delete-file "~/scratch")
      (make-symbolic-link scratch "~/scratch" t))
    (when git
      (require 'vc-git)
      (let ((default-directory scratch))
        (vc-git-create-repo)))
    (dired scratch)))

(defun vifon/image-yank ()
  (interactive)
  (when (or (zerop (buffer-size))
            (y-or-n-p "Buffer not empty. Proceed?"))
    (erase-buffer)
    (call-process "xclip" nil t nil "-selection" "clipboard" "-t" "image/png" "-o")
    (set-buffer-file-coding-system 'raw-text)
    (save-buffer)
    (normal-mode)
    (when (y-or-n-p "Kill the buffer?")
      (kill-buffer))))

(defun vifon/shrink-all-windows-if-larger-than-buffer ()
  (interactive)
  (mapc #'shrink-window-if-larger-than-buffer (window-list)))

(defun ansi-colorize-buffer ()
  (interactive)
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region (point-min) (point-max)))
  (set-buffer-modified-p nil))

;; Autoload `grep-mode' as an interactive command (it's already
;; autoloaded as a function) to allow enabling it by hand.
(autoload 'grep-mode "grep" nil t)

(defun ansi-color-grep-mode ()
  (interactive)
  (ansi-colorize-buffer)
  (grep-mode))

(defun vifon/straight-pull-and-rebuild-package (package)
  (interactive
   (list (straight--select-package "Pull and rebuild package"
                                   #'straight--installed-and-buildable-p)))
  (straight-pull-package package)
  (straight-rebuild-package package))

(defun vifon/recentf-directories ()
  (interactive)
  (dired (completing-read "Recent directories: "
                          (mapcar #'file-name-directory recentf-list))))

(defun vifon/desktop-save-with-tramp ()
  "Like `desktop-save' but sets `desktop-files-not-to-save' to nil."
  (interactive)
  (let ((desktop-files-not-to-save nil))
    (call-interactively #'desktop-save)))

(defun vifon/shellcheck-disable-at-point ()
  "Add a Shellcheck annotation for the problem at point."
  (interactive)
  (let ((warning (flymake--diag-text
                  (get-char-property (point) 'flymake-diagnostic))))
    (if (string-match "\\`\\(SC[0-9]\\{4\\}\\)\\b" warning)
        (setq warning (match-string 1 warning))
      (user-error "Invalid Shellcheck warning: %s" warning))
    (save-excursion
      ;; Naive but works.
      (backward-paragraph)
      (newline-and-indent)
      (insert (format "# shellcheck disable=%s" warning)))))
