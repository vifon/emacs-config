;;; -*- lexical-binding: t; -*-

(use-package tmsu
  :if (executable-find "tmsu")
  :straight t
  :defer t
  :config (progn
            (add-hook 'tmsu-tag-list-preprocess-functions
                      #'vifon/tmsu-sort-priority-tags-last)
            (advice-add 'tmsu-edit :around
                        #'vifon/call-without-orderless-dispatchers)))

(defconst vifon/tmsu-episode-count-tag "episodes-watched")

(defcustom vifon/tmsu-priority-tags (mapcar (lambda (x) (concat x "="))
                                            (list "status"
                                                  vifon/tmsu-episode-count-tag
                                                  "rating"))
  "Tags to always sort as last for easy editing in `tmsu-edit'.

The first one on this list gets displayed last.

Each value is a tag prefix to match, so tags with values should
end with =."
  :type '(repeat string))

(defun vifon/tmsu-sort-priority-tags-last (tags)
  "Sort `vifon/tmsu-priority-tags' as the last tags for easy editing."
  (sort tags
        (lambda (a b)
          (catch 'sorted
            (dolist (tag vifon/tmsu-priority-tags)
              (when (string-prefix-p tag a)
                (throw 'sorted nil))
              (when (string-prefix-p tag b)
                (throw 'sorted t)))
            (string< a b)))))


(use-package tmsu-dired
  :after dired
  :if (executable-find "tmsu")
  :bind ( :map dired-mode-map
          (";" . tmsu-dired-edit-directory)
          ("M-;" . tmsu-dired-query)
          ("C-M-;". tmsu-dired-overlay))
  :config (advice-add 'tmsu-dired-query :around
                      #'vifon/call-without-orderless-dispatchers))
