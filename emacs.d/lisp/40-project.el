;;; -*- lexical-binding: t; -*-

(use-package project
  :straight t
  :defer t
  :config (setq project-switch-commands #'project-dired))

(use-package project-fossil
  :after project)
