;;; -*- lexical-binding: t; -*-

(use-package deft
  :straight t
  :bind (("<f5>" . deft)
         :map deft-mode-map
         ("<f5>" . deft-index))
  :init (defun deft-index ()
          (interactive)
          (dired deft-directory))
  :config (progn
            (setq deft-auto-save-interval 0
                  deft-default-extension "org"
                  deft-new-file-format "%Y%m%dT%H%M%S--new"
                  deft-use-filter-string-for-filename nil
                  deft-file-naming-rules '((noslash . "-")
                                           (nospace . "-")
                                           (case-fn . downcase)))
            (define-key deft-mode-map (kbd "C-c C-n") #'zettel2-create-note)))

(use-package zettel2
  :straight ( :host github :repo "vifon/zettel2"
              :files (:defaults "graph.pl"))
  :after org
  :config (progn
            (require 'zettel2-link)
            (setq zettel2-graph-format "png")))

(use-package markdown-mode
  :straight t
  :hook (markdown-mode . my-markdown-mode-hook)
  :config (defun my-markdown-mode-hook ()
            (add-to-list (make-local-variable 'electric-pair-pairs)
                         '(?` . ?`))))

(use-package edit-indirect
  :straight t
  :defer t)

(use-package hyperlist-mode
  :straight t
  :mode ("\\.hl\\'" . hyperlist-mode))
