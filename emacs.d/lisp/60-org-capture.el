;;; -*- lexical-binding: t; -*-

(defgroup vifon/org-capture nil
  "A personal `org-capture' setup.")

(defconst vifon/org-capture-templates-file-regexp
  (rx string-start
      (= 2 digit)
      "-"
      (one-or-more any)
      ".eld"
      string-end)
  "A regexp matching files like 10-name.eld")

(defcustom vifon/org-capture-templates-directory
  (no-littering-expand-etc-file-name "org-capture-templates")
  "The directory to load the templates from.")

(defun vifon/org-capture-templates-reload (&optional directory keep)
  "Load or reload `org-capture-templates' from multiple files.

DIRECTORY defaults to `vifon/org-capture-templates-directory'.

Provide KEEP (\\[universal-argument]) to append to the current
list, not overwrite."
  (interactive (list
                (read-directory-name "Template directory: "
                                     vifon/org-capture-templates-directory
                                     nil t)
                current-prefix-arg))
  (setq directory (or directory vifon/org-capture-templates-directory))
  (unless keep
    (setq org-capture-templates nil))
  (setq org-capture-templates
        (append org-capture-templates
                (mapcan (lambda (file)
                          (with-temp-buffer
                            (insert-file-contents file)
                            (read (current-buffer))))
                        (directory-files
                         (file-name-as-directory directory)
                         t vifon/org-capture-templates-file-regexp)))))

(use-package org-capture
  :bind ("C-c c" . org-capture)
  :config (progn
            (vifon/org-capture-templates-reload)
            (setq org-capture-templates-contexts
                  '(("m" ((in-mode . "notmuch-show-mode")))
                    ("ts" ((lambda () (or (org-clocking-p)
                                          (derived-mode-p 'org-mode)))))
                    ("tb" ((lambda () (and (derived-mode-p 'prog-mode)
                                           (project-current)))))
                    ("n" ((lambda () (org-clocking-p))))
                    ("n" "N" ((lambda () (not (org-clocking-p)))))))))
