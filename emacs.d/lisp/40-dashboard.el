;;; -*- lexical-binding: t; -*-

(use-package scratch-mode
  :straight ( :host github :repo "vifon/scratch-mode"
              :branch "experimental")
  :bind (("C-c s" . scratch-reset)
         :map scratch-mode-map
         ("," . vifon/theme-light)
         ("." . vifon/theme-dark)
         ("/" . vifon/theme-dwim)
         ("b" . switch-to-buffer)
         ("k" . bookmark-jump)
         ("f" . find-file)
         ("P" . straight-use-package)
         ("R" . recentf-edit-list)
         ("c" . calc))
  :init (setq initial-major-mode 'scratch-mode)
  :config (progn
            (define-key scratch-mode-map (kbd "S")
                        (cons
                         "make-scratch-dir"
                         #'vifon/make-scratch-dir))
            (define-key scratch-mode-map (kbd "C")
                        (cons
                         "chronos"
                         (lambda ()
                           (interactive)
                           (unless (bound-and-true-p chronos--buffer)
                             (require 'chronos)
                             (chronos-initialize))
                           (switch-to-buffer chronos--buffer))))
            (dolist (key '(?C ?S ?R))
              (add-to-list 'scratch-mode-key-hints key 'append))
            (define-key scratch-mode-map (kbd "l")
                        (scratch-browse-directory-binding
                         (expand-file-name "lisp/"
                                           user-emacs-directory)))
            (let ((local-lisp (expand-file-name "local.d/"
                                                user-emacs-directory)))
              (when (file-directory-p local-lisp)
                (define-key scratch-mode-map (kbd "L")
                            (scratch-browse-directory-binding local-lisp))))
            (vifon/add-to-list-after 'scratch-mode-key-hints ?I ?l)
            (vifon/add-to-list-after 'scratch-mode-key-hints ?l ?L)
            (vifon/add-to-list-after 'scratch-mode-key-hints ?e ?\()))
