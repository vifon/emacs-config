;;; -*- lexical-binding: t; -*-

(autoload 'tramp-cleanup-all-buffers "tramp-cmds")

(use-package transient
  :commands (vifon/dired-transient)
  :bind ("C-c t" . vifon-transient)
  :config
  (setq transient-save-history nil)

  (transient-define-prefix vifon-transient ()
    "My main personal transient dispatcher."
    [["Cleanup"
      ("wc" "Whitespace cleanup" whitespace-cleanup)
      ("aa" "Align" align)
      ("ar" "Align regexp" align-regexp)
      ("ss" "Sort lines" sort-lines)
      ("sr" "Reverse lines" reverse-region)]
     ["Toggles"
      ("v" "Visual line mode" visual-line-mode :transient t)
      ("r" "Truncate lines" toggle-truncate-lines :transient t)
      ("ln" "Display line numbers" display-line-numbers-mode :transient t)
      ("hl" "Highlight current line" hl-line-mode :transient t)
      ("ww" "Whitespace mode" whitespace-mode :transient t)
      ("gf" "Display fill indicator" display-fill-column-indicator-mode :transient t)
      ("fl" "Follow mode" follow-mode :transient t)
      "3rd party toggles"
      ("O" "Olivetti" olivetti-mode :transient t)
      ("gi" "Indent guide mode" highlight-indent-guides-mode :transient t)]
     ["Others"
      ("c" "compile…" vifon/compile-transient)
      ("!" "Emacs debug" vifon/emacs-debug-transient :transient transient--do-exit)
      ("F" "fly*-modes…" vifon/fly*-transient)
      ("o" "compare" compare-windows :transient t)
      ("nm" "normal-mode" normal-mode)
      ("tc" "TRAMP shutdown" tramp-cleanup-all-buffers)
      ("D" "Session management" vifon/session-management-transient
       :transient transient--do-exit)]])

  (transient-define-prefix vifon/emacs-debug-transient ()
    "Commands related to debugging of Emacs."
    [["General debugging"
      ("!" "Debug on error" toggle-debug-on-error :transient t)
      ("g" "Debug on quit" toggle-debug-on-quit :transient t)]
     ["Fine-grained debugging"
      ("f" "Debug on entry" debug-on-entry :transient t :level 3 )
      ("F" "Cancel debug on entry" cancel-debug-on-entry :transient t :level 3)
      ("v" "Debug on variable change" debug-on-variable-change :transient t :level 4)
      ("V" "Cancel debug on variable change" cancel-debug-on-variable-change :transient t :level 4)]
     ["Tracing"
      ("t" "Trace function" trace-function :transient t :level 2)
      ("b" "Trace function (background)" trace-function-background :transient t :level 2)
      ("T" "Untrace function" untrace-function :transient t :level 2)]])

  (transient-define-prefix vifon/session-management-transient ()
    "Emacs session management."
    [["desktop.el"
      ("l" "Load session" desktop-change-dir)
      ("s" "Save session" desktop-save)
      ("S" "Save session + TRAMP" vifon/desktop-save-with-tramp)
      ("c" "Clear dession" desktop-clear)]])

  (transient-define-prefix vifon/compile-transient ()
    "Compilation related commands."
    [["Compilation"
      ("c" "compile" compile)
      ("r" "recompile" recompile)]
     [ :if-derived emacs-lisp-mode
       :description "Elisp"
       ("e" "eval-buffer" eval-buffer)]])

  (transient-define-prefix vifon/fly*-transient ()
    "Flymake, flyspell and the related commands."
    [["fly*-modes"
      ("sm" "Flyspell prog mode" flyspell-prog-mode :if-derived prog-mode :transient t)
      ("sm" "Flyspell mode" flyspell-mode :if-not-derived prog-mode :transient t)
      ("sb" "Flyspell buffer" flyspell-buffer)]
     [""
      ("i" "Ispell change dictionary" ispell-change-dictionary :transient t)]])

  (autoload 'gnus-dired-mode "gnus-dired" nil t)
  (transient-define-prefix vifon/dired-transient ()
    "dired commands and submodes"
    [["Modes"
      ("a" "async" dired-async-mode :transient t)
      ("c" "Collapse mode" dired-collapse-mode :transient t)
      ("m" "Attachment mode" gnus-dired-mode :transient t)
      ("v" "diff-hl" (lambda ()
                       (interactive)
                       (call-interactively #'diff-hl-dired-mode)
                       (revert-buffer))
       :transient t)]
     ["Search"
      ("ff" "find-dired" find-dired)
      ("fn" "find-name-dired" find-name-dired)
      ("fg" "find-grep-dired" find-grep-dired)]]))
