;;; -*- lexical-binding: t; -*-

(defcustom vifon/tmsu-dired-episode-rx
  (rx (+ digit)
      (? "." (+ digit)))
  "Regexp extracting the episode's number from the filename.

This value is being embedded into a larger regexp by
`vifon/tmsu-dired--build-rx'.

Intended to be overridden with `.dir-locals.el' as necessary."
  :group 'tmsu-dired
  :type 'string
  :safe #'stringp)

(defcustom vifon/tmsu-dired-season-rx
  (rx "S" (+ digit)
      (| " - "
         "E"))
  "Regexp matching a season prefix before the episode number.

This value is being embedded into a larger regexp by
`vifon/tmsu-dired--build-rx'.

Intended to be overridden with `.dir-locals.el' as necessary."
  :group 'tmsu-dired
  :type 'string
  :safe #'stringp)

(defun vifon/tmsu-dired--build-rx (&optional name-regexp)
  "Build a final regexp using the previously defined partial regexps.

The exact matching semantics can be controlled with the
`vifon/tmsu-dired-episode-rx' and
`vifon/tmsu-dired-season-rx' variables.

If NAME-REGEXP is provided, it's used instead of
`vifon/tmsu-dired-episode-rx'.

Depending on NAME-REGEXP being passed or not, the capture groups
have different meaning.  See the comments inside the
function definition."
  (if name-regexp
      ;; A regexp for finding a specific episode.  The first capture
      ;; group matches greedily to include any season numbers not
      ;; already included in the episode number (NAME-REGEXP), so that
      ;; "S2 - 03" won't be matched as a valid next episode after
      ;; "02".  This group should be inspected by the caller and
      ;; a candidate rejected accordingly.  A negative lookbehind
      ;; would work better but Emacs doesn't support these (yet?).
      (rx (*? any)
          (? (group (regexp vifon/tmsu-dired-season-rx)))
          word-boundary
          (regexp name-regexp)
          ;; Revision number.
          (? "v" digit)
          word-boundary
          (* any)
          (regexp vifon/media-video-extension-rx))
    ;; A regexp for finding any valid episode and extracting the data
    ;; from the filename.
    (rx word-boundary
        (group (? (regexp vifon/tmsu-dired-season-rx))
               (regexp vifon/tmsu-dired-episode-rx))
        ;; Revision number.
        (? "v" digit)
        word-boundary
        (* any)
        (regexp vifon/media-video-extension-rx))))

(defun vifon/tmsu-dired-watch-next-episode (&optional start-at-point interactive)
  "Find the next episode of the media in the current directory.

The START-AT-POINT prefix argument ignores the last watched
episode and unconditionally opens the file at point (or the next
applicable one).  It's useful for manual intervention in some
nasty corner cases with overlapping file naming schemes
between seasons.

INTERACTIVE is used purely as a marker for interactive calls.
If it's nil, the interactive prompts are disabled."
  (interactive "P\np" dired-mode)
  (require 'tmsu)
  (unless (tmsu-database-p)
    (error "No TMSU database"))
  (let* ((dir (dired-current-directory))
         (tag vifon/tmsu-episode-count-tag)
         ;; Determine the last watched episode.
         (curr-ep (or (tmsu-tag-value (car-safe (tmsu-get-tags dir tag)))
                      (unless (and interactive
                                   (yes-or-no-p
                                    (format-message
                                     "`%s' isn't set.  Initialize? " tag)))
                        (user-error "`%s' isn't set." tag)))))
    (let ((max (1- (dired-subdir-max))))
      (unless start-at-point
        ;; Search within the boundaries of the current dired subdir.
        (goto-char (dired-subdir-min))
        (when curr-ep
          ;; If we already have a previous watched episode, go through
          ;; the files up until one matches the regexp or we're out
          ;; of bounds.
          (vifon/tmsu-dired-goto-episode curr-ep max)))

      ;; Move to the next matching file after the last watched one.
      ;; Parse the new episode's filename to obtain the new value to
      ;; save in the tag.
      (let ((next-ep (vifon/tmsu-dired-find-next-episode
                      (and (not start-at-point) curr-ep)
                      max)))
        ;; Move the point to a nice and tidy place if possible.
        (dired-move-to-filename)

        (if (not next-ep)
            (if (and interactive
                     curr-ep
                     (yes-or-no-p
                      "No next episode found, unset tag? "))
                (tmsu-tags-remove dir (concat tag "=" curr-ep))
              (user-error "No next episode found"))
          (make-process :name "mpv"
                        :command `("mpv" "--" ,(dired-get-file-for-visit))
                        :buffer (current-buffer)
                        :filter #'ignore
                        :sentinel (lambda (proc state)
                                    (kill-local-variable 'mode-line-process)
                                    (delete-process proc)
                                    (force-mode-line-update)
                                    (when (and interactive
                                               (y-or-n-p
                                                (format-message
                                                 "Increment the `%s=%s' tag to `%s'? "
                                                 tag (or curr-ep "") next-ep)))
                                      (message "Bumping to `%s=%s' (was `%s')"
                                               tag next-ep (or curr-ep ""))
                                      (tmsu-tags-remove dir (concat tag "=" curr-ep))
                                      (tmsu-tags-add    dir (concat tag "=" next-ep)))))
          (setq mode-line-process '(":mpv:%s")))))))

(defun vifon/tmsu-dired-goto-episode (episode &optional max)
  "Find a specific EPISODE below point, up until the MAX position.

Leaves the point at the found file."
  (setq max (or max (1- (dired-subdir-max))))
  ;; Regexp matching specifically the current episode, possibly with
  ;; leading zeroes.
  (let ((regexp (vifon/tmsu-dired--build-rx
                 (concat "0*" (regexp-quote episode)))))
    (while (and (not (let ((file (dired-get-filename t t)))
                       (and file
                            (string-match regexp file)
                            (not (match-string 1 file)))))
                (< (point) max))
      (forward-line 1))))

(defun vifon/tmsu-dired-find-next-episode (&optional last-episode max)
  "Find the next episode below point, up until the MAX position.

LAST-EPISODE is the last episode, possibly nil.

Return the next episode's number according to `vifon/tmsu-dired-episode-rx'."
  (setq max (or max (1- (dired-subdir-max))))
  (cl-do* ((regexp (vifon/tmsu-dired--build-rx))
           (file (unless last-episode
                   ;; If there was no last episode, consider the
                   ;; current line too.
                   (dired-get-filename t t))
                 (dired-get-filename t t))
           (next-ep (and file
                         (string-match regexp file)
                         (match-string 1 file))
                    (and file
                         (string-match regexp file)
                         (match-string 1 file))))
      ;; When either `next-ep' is true or we're out of bounds…
      ((or next-ep
           (>= (point) max))
       ;; …return the cleaned up `next-ep'.
       (when next-ep
         (string-trim-left next-ep "0+")))
    ;; …otherwise try the next line.
    (forward-line 1)))

(defun vifon/tmsu-create-media-directory (directory)
  "Create a new DIRECTORY for TMSU-tagged media and tag it as ongoing."
  (interactive
   (list (read-file-name "Create media directory: " (dired-current-directory))))
  (require 'tmsu)
  (dired-create-directory directory)
  (let ((current-year (format-time-string "%Y"))
        (new-directory (dired-get-file-for-visit)))
    (tmsu-tags-add new-directory
                   (concat "year=" current-year)
                   "status=ongoing")
    (tmsu-edit new-directory)))
