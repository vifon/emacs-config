;;; -*- lexical-binding: t; -*-

(global-subword-mode 1)
(diminish 'subword-mode)
(electric-pair-mode 1)
(electric-indent-mode 1)

;; Added in Emacs 30.
(when (fboundp 'kill-ring-deindent-mode)
  (kill-ring-deindent-mode 1))

(diminish 'eldoc-mode)

(setq tags-revert-without-query t
      tags-add-tables nil)

(setq bidi-display-reordering nil)

(tooltip-mode 0)

(setq inhibit-startup-screen t)

(column-number-mode t)

(setq auto-hscroll-mode 'current-line)

(setq bookmark-set-fringe-mark nil)

;; Make the newly created frames a little wider.
(setf (alist-get 'width default-frame-alist nil t) 120)

(setq Info-scroll-prefer-subnodes t)

(setq async-shell-command-display-buffer nil)

(setq delete-pair-blink-delay 0)

(setq echo-keystrokes 0.1)

(blink-cursor-mode 0)
(setq visible-cursor nil)
(setq scroll-step 10
      next-screen-context-lines 10)
(mouse-avoidance-mode 'none)

(setq ediff-grab-mouse nil
      ediff-window-setup-function 'ediff-setup-windows-plain)

(setf (alist-get "\\*Calendar\\*" display-buffer-alist nil t #'equal)
      '(display-buffer-below-selected))


;; Never timeout the TRAMP sessions, it causes Emacs to hang too
;; often.  I'd rather manage their lifetime myself.  This timeout got
;; added in Emacs 27.
(setq tramp-connection-properties '((nil "session-timeout" nil)))

;; Prevent TRAMP from accessing encrypted auth-sources files which are
;; unlikely to contain any relevant credentials anyway.
;; These password prompts are unhelpful and merely distruptive.
(setq tramp-completion-use-auth-sources nil)
(connection-local-set-profile-variables
 'remote-without-auth-sources '((auth-sources . nil)))
(connection-local-set-profiles
 '(:application tramp) 'remote-without-auth-sources)

;; See: (info "(tramp) Ad-hoc multi-hops")
(setq tramp-show-ad-hoc-proxies t)

(setq eshell-hist-ignoredups t)

(setq vc-follow-symlinks t)
(when (equal system-type 'gnu/linux)
  (setq x-select-enable-primary t
        select-active-regions nil
        mouse-drag-copy-region t
        x-select-enable-clipboard t))

(setq mouse-drag-mode-line-buffer t)

;; Turn off tabs
(setq-default indent-tabs-mode nil)

;; Set the tab width
(setq-default default-tab-width 4
              tab-width 4
              c-basic-indent 4
              c-basic-offset 4)
(with-eval-after-load 'cc-vars
  (setq c-default-style (copy-alist c-default-style))
  (setf (alist-get 'other c-default-style)
        "k&r"))
(c-set-offset 'inline-open 0)
(c-set-offset 'access-label -2)
(c-set-offset 'innamespace 0)
(setq css-indent-offset 2)

(setq parens-require-spaces nil)

(setq fill-nobreak-predicate '(fill-single-word-nobreak-p
                               fill-single-char-nobreak-p))

(setq require-final-newline t)
(setq-default indicate-buffer-boundaries 'left)

(add-hook 'prog-mode-hook
          (lambda () (setq show-trailing-whitespace t)))

(setq password-cache-expiry 300)

(setq browse-url-browser-function #'browse-url-generic
      browse-url-secondary-browser-function #'browse-url-generic)

(when (file-directory-p "~/dl")
  (setq eww-download-directory "~/dl"))
(setq browse-url-generic-program
      (cond
       ((executable-find "webbroboard")
        "webbroboard")
       ((and
         (string= (getenv "XDG_SESSION_TYPE") "wayland")
         (executable-find "wl-copy"))
        "wl-copy")))

(add-to-list 'auto-mode-alist '("\\.ebuild\\'" . sh-mode))
(add-to-list 'auto-mode-alist '("/Pkgfile\\'" . sh-mode))

(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

(setq epa-file-name-regexp "\\.gpg\\(~\\|\\.~[0-9]+~\\)?\\'\\|\\.asc")
(epa-file-name-regexp-update)

(setq calendar-date-style 'iso
      calendar-mark-diary-entries-flag nil
      calendar-mark-holidays-flag nil
      calendar-week-start-day 1)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(put 'erase-buffer 'disabled nil)
(put 'set-goal-column 'disabled nil)

;; My window managers usually don't play nice with iconifying Emacs.
;; Let's disable it but save the original function under a different
;; name.
(fset 'iconify-orig (symbol-function 'iconify-frame))
(defalias 'iconify-frame 'ignore)

(defalias 'elpa 'package-list-packages)
