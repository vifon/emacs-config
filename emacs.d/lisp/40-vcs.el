;;; -*- lexical-binding: t; -*-

(use-package magit
  :straight t
  :defer t
  :config (progn
            (setq magit-diff-refine-hunk t
                  magit-status-goto-file-position t
                  magit-delete-by-moving-to-trash nil)
            (setq magit-repository-directories
                  '(("~/configs/" . 1)
                    ("~/projects/" . 2)))
            (vifon/add-to-list-after 'magit-repolist-columns
                                     '("Version")
                                     '("D" 1 magit-repolist-column-flag ())
                                     (lambda (a b)
                                       (string= (car a)
                                                (car b))))
            (transient-replace-suffix 'magit-commit 'magit-commit-autofixup
              '("x" "Absorb changes" magit-commit-absorb))
            (dolist (keymap (list magit-status-mode-map
                                  magit-log-mode-map
                                  magit-reflog-mode-map
                                  magit-refs-mode-map
                                  magit-diff-mode-map)))
            (add-hook 'magit-pre-refresh-hook #'diff-hl-magit-pre-refresh)
            (add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)))

(use-package magit-todos :straight t :defer t)

(use-package forge
  :straight t
  :after magit)

(use-package git-commit
  :after magit
  :mode ("/COMMIT_EDITMSG\\'" . git-commit-mode)
  :bind ( :map git-commit-mode-map
          ("C-c C-l" . magit-log)))

(use-package git-timemachine
  :straight t
  :bind ("C-x v t" . git-timemachine))

(use-package git-link
  :straight t
  :bind ("C-x v w" . git-link))

(use-package diff-hl
  :straight t
  :defer 2
  :bind ("C-x v \\" . diff-hl-amend-mode)
  :commands (diff-hl-magit-pre-refresh
             diff-hl-magit-post-refresh)
  :config (global-diff-hl-mode 1))
