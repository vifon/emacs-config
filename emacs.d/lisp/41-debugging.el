;;; -*- lexical-binding: t; -*-

(use-package dape
  :straight t
  :commands dape)
