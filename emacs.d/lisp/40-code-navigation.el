;;; -*- lexical-binding: t; -*-

(use-package treemacs
  :straight t
  :bind (("C-c b" . treemacs)
         :map treemacs-mode-map
         ("j" . treemacs-next-line)
         ("k" . treemacs-previous-line)
         ("W" . treemacs-switch-workspace)))

(use-package dumb-jump
  :straight t
  :hook ((cperl-mode
          c-mode-common
          sh-mode)
         . dumb-jump-activate)
  :init (defun dumb-jump-activate ()
          (interactive)
          (add-hook 'xref-backend-functions
                    #'dumb-jump-xref-activate
                    nil t)))

(use-package eglot
  :straight t
  :hook (( python-mode python-ts-mode
           go-mode go-ts-mode
           rust-mode rust-ts-mode)
         . eglot-ensure)
  :bind ( :map eglot-mode-map
          ("C-c v" . eglot-code-actions)))

(use-package rg
  :straight t
  :commands rg
  :bind ("M-s ," . rg-dwim))

(use-package deadgrep
  :straight t
  :commands deadgrep)

(use-package topsy
  :straight t
  :defer t)


;;; Code folding:

(defun vifon/toggle-selective-display (arg)
  (interactive "P")
  (if arg
      (set-selective-display arg)
    (set-selective-display (and (zerop (or selective-display 0))
                                (not (zerop (current-column)))
                                (current-column)))))
(bind-key [remap set-selective-display] #'vifon/toggle-selective-display)


(defun vifon/yaml-outline-enable ()
  (interactive)
  (setq-local outline-regexp
              (rx
               (or
                ;; Document separator for multi-document files.
                (: "---" eol)
                (:
                 (* (syntax whitespace))
                 (or
                  ;; Require at least one regular character, to exclude
                  ;; empty lines.  They would be considered top-level
                  ;; headings otherwise, or lower if more whitespace is
                  ;; added to the empty line full of whitespace.
                  ;; The empty line headings also seem to mess with the
                  ;; RET keybind in the overlay keymap applied to the
                  ;; beginning of each outline heading.
                  (or (syntax word)
                      (syntax string-quote))
                  ;; Account for the non-indented arrays.
                  ;; Their `outline-level' (aka the match length) needs
                  ;; to be higher than the parent element's, which is
                  ;; already true if we include the hyphen that follows
                  ;; the whitespace (and one following whitespace
                  ;; character since we also count the first character
                  ;; in the other cases).
                  (: "-" (syntax whitespace)))))))
  (setq-local outline-heading-alist '(("---" . 0)))
  (outline-minor-mode 1))

(use-package outline
  :commands outline-minor-mode
  :hook (yaml-mode . vifon/yaml-outline-enable)
  ;; Enable `outline-minor-mode' before other hooks.
  ;; Specifically before `hs-minor-mode' so its keymap can
  ;; take priority.
  :init (add-hook 'prog-mode-hook #'outline-minor-mode -1)
  :config (setq outline-minor-mode-use-buttons 'in-margins))


(use-package hideshow
  :hook (prog-mode . hs-minor-mode)
  :bind ( :map hs-minor-mode-map
          ("M-RET" . hs-toggle-hiding)))
