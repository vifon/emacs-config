;;; -*- lexical-binding: t; -*-

(use-package apheleia
  :straight t
  :hook (( python-mode python-ts-mode
           go-mode go-ts-mode
           rust-mode rust-ts-mode
           c-mode-common
           js-mode)
         . apheleia-mode)
  :config (progn
            (setq apheleia-mode-alist (copy-alist apheleia-mode-alist))
            (setf (alist-get 'python-mode apheleia-mode-alist)
                  '(isort black))
            (setf (alist-get 'python-ts-mode apheleia-mode-alist)
                  '(isort black))
            (setq apheleia-remote-algorithm 'local)))

(use-package flymake
  :straight t
  :hook (sh-mode . flymake-mode))

(use-package flyspell
  :hook (git-commit-mode . flyspell-mode))
