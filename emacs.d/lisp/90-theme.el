;;; -*- lexical-binding: t; -*-

(defvar vifon/theme-light 'modus-operandi)
(defvar vifon/theme-dark  'modus-vivendi)
(defvar vifon/font-name "JetBrains Mono")
(defvar vifon/font-size "13")


(defun vifon/set-font ()
  (interactive)
  (ignore-errors
    (let ((font (concat vifon/font-name "-" vifon/font-size)))
      (add-to-list 'default-frame-alist `(font . ,font))
      (set-frame-font font nil t))))

(if (fboundp 'consult-theme)
    (defalias 'vifon/switch-theme 'consult-theme)
  (defun vifon/switch-theme (new-theme)
    "Change the current theme to NEW-THEME, disabling other themes."
    (interactive
     (list
      (intern (completing-read "Load custom theme: "
                               (mapcar #'symbol-name
				                       (custom-available-themes))))))
    (mapc #'disable-theme custom-enabled-themes)
    (load-theme new-theme 'no-confirm)))

(defun vifon/theme-light ()
  "Switch to the preferred light theme."
  (interactive)
  (vifon/switch-theme vifon/theme-light))

(defun vifon/theme-dark ()
  "Switch to the preferred dark theme."
  (interactive)
  (vifon/switch-theme vifon/theme-dark))

(bind-key "C-M-s-<" #'vifon/theme-light)
(bind-key "C-M-s->" #'vifon/theme-dark)

(use-package zenburn-theme
  :straight t)

(use-package modus-themes
  :straight t
  :config (setq modus-themes-mode-line '()
                modus-themes-region '(no-extend)
                modus-themes-fringes 'subtle
                modus-themes-italic-constructs t
                modus-themes-bold-constructs t
                modus-themes-syntax '()
                modus-themes-hl-line '()
                modus-themes-paren-match '()))


(defun vifon/solar-times ()
  (require 'seq)
  (require 'solar)
  (if (and calendar-latitude
           calendar-longitude)
      (mapcar (lambda (time)
                (cons
                 (string-to-number
                  (let ((calendar-time-display-form '(24-hours minutes)))
                    (apply #'solar-time-string time)))
                 (let ((calendar-time-display-form '(24-hours ":" minutes)))
                   (apply #'solar-time-string time))))
              (seq-take (solar-sunrise-sunset (calendar-current-date))
                        2))
    '((900 . "09:00")
      (1700 . "17:00"))))

(defun vifon/daytime-solar-p ()
  "Check whether it's daytime according to the calculated sunrise
& sunset times.

See: Info node `(emacs) Sunrise/Sunset'."
  (let* ((sunrise-sunset (mapcar #'car (vifon/solar-times)))
         (sunrise (car sunrise-sunset))
         (sunset (cadr sunrise-sunset))
         (now (string-to-number (format-time-string "%H%M"))))
    (< sunrise
       now
       sunset)))

(defun vifon/daytime-p ()
  "Check whether it's daytime according to the hardcoded times."
  (let ((now (string-to-number (format-time-string "%H%M"))))
    (< 0800
       now
       1800)))

(defvar vifon/theme-timers nil)

(defun vifon/theme-schedule-timers ()
  "Schedule the timers for automatic theme switching."
  (mapc #'cancel-timer vifon/theme-timers)
  (let ((24h (* 24 60 60))
        (sunrise-sunset (mapcar #'cdr (vifon/solar-times))))
    (setq vifon/theme-timers
          (cl-mapcar
           (lambda (time theme)
             (run-at-time time 24h theme))
           sunrise-sunset
           '(vifon/theme-light
             vifon/theme-dark)))))

(defun vifon/theme-dwim (&optional interactive)
  (interactive "p")
  (if interactive
      (call-interactively #'vifon/switch-theme)
    (if (vifon/daytime-solar-p)
        (vifon/theme-light)
      (vifon/theme-dark))))

(defun vifon/theme-startup ()
  ;; Schedule only once per Emacs launch.  It shouldn't diverge that
  ;; much during the Emacs instance lifetime for that to matter to
  ;; recalculate the solar times.
  (vifon/theme-schedule-timers)
  (run-at-time "0:00" nil
               #'vifon/theme-dark)
  (if (daemonp)
      (progn
        (defun vifon/theme-init-daemon (frame)
          (with-selected-frame frame
            (vifon/theme-dwim))
          ;; Run this hook only once.
          (remove-hook 'after-make-frame-functions
                       #'vifon/theme-init-daemon)
          (fmakunbound 'vifon/theme-init-daemon))
        (add-hook 'after-make-frame-functions
                  #'vifon/theme-init-daemon))
    (vifon/theme-dwim)))

(bind-key "C-M-s-?" #'vifon/theme-dwim)

(vifon/set-font)

(cond
 ((getenv "EMACS_NO_THEME")
  ;; Skip any theme initialization.
  )
 ((daemonp)
  ;; Only the main Emacs instance should have auto theme switching.

  ;; Emacs started as a daemon needs to have its font set after each
  ;; theme switch.  Needs further investigation.
  (advice-add 'vifon/switch-theme :after (lambda (&rest _args) (vifon/set-font)))
  (vifon/theme-startup))
 ((display-graphic-p)
  ;; Any secondary Emacs instance should be immediately recognizable.
  (vifon/switch-theme 'zenburn))
 (t
  ;; The dark theme looks quite nice in the terminal Emacs instances.
  ;; Rarely used, but let's support it.
  (vifon/theme-dark)))
