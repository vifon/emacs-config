;;; -*- lexical-binding: t; -*-

(defun vifon/add-to-list-after (list-var old new &optional compare-fn)
  "Add NEW after OLD in the ordered list LIST-VAR.

OLD is compared with COMPARE-FN which defaults to `equal'.

NEW is not added if it already exists after OLD, also according
to COMPARE-FN, making this function idempotent.

Only the first occurrence of OLD is taken into account."
  (let ((cmp (or compare-fn #'equal)))
    (cl-do ((x (symbol-value list-var) (cdr x)))
        ((null x))
      (when (and (funcall cmp (car x) old)
                 (not (funcall cmp (cadr x) new)))
        (setf (cdr x) (cons new (cdr x)))
        (cl-return))))
  (symbol-value list-var))

(defun vifon/buffer-file-or-directory-name (buf)
  "The file BUF is visiting, works also if it's a `dired' buffer."
  (with-current-buffer buf
    (or buffer-file-name
        (and (eq major-mode 'dired-mode)
             (boundp 'dired-directory)
             (file-name-directory
              (if (stringp dired-directory)
                  dired-directory
                (car dired-directory)))))))

(defconst vifon/media-video-extension-rx
  (rx "."
      (| "mkv"
         "mp4"
         "avi"
         "m4v"
         "webm")
      eos)
  "Regexp matching the common video file extensions.")

(defconst vifon/media-audio-extension-rx
  (rx "."
      (| "mp3"
         "ogg"
         "opus")
      eos)
  "Regexp matching the common audio file extensions.")
