;;; -*- lexical-binding: t; -*-

(use-package visual-regexp
  :straight t
  :commands vr/query-replace)

(use-package kmacro-x
  :straight t
  :bind (("C-<" . kmacro-x-mc-mark-previous)
         ("C->" . kmacro-x-mc-mark-next)
         ("M-<mouse-1>" . kmacro-x-mc-mark-at-click)
         ("M-<down-mouse-1>" . nil)
         ("C-M-:" . kmacro-x-mc-legacy)
         :map kmacro-x-mc-mode-map
         ("C-v" . kmacro-x-mc-pause))
  :custom ((kmacro-x-atomic-undo-mode t)
           (kmacro-x-mc-atomic-undo-mode t))
  :diminish kmacro-x-atomic-undo-mode)

(use-package wgrep :straight t :defer t)
