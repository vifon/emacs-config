;;; -*- lexical-binding: t; -*-

(use-package dired
  :bind ( :map dired-mode-map
          ("h" . dired-up-directory)
          ("z" . dired-subtree-toggle)
          ("TAB" . vifon/dired-transient)
          ("I" . vifon/dired-insert-subdir-keep-point)
          ("E" . vifon/dired-wl-copy)
          ([remap back-to-indentation] . vifon/dired-move-to-filename)
          ([remap find-file] . vifon/find-file-from-dired))
  :config (progn
            (setq dired-ls-F-marks-symlinks t
                  dired-isearch-filenames 'dwim
                  wdired-allow-to-change-permissions t
                  image-dired-external-viewer "sxiv"
                  dired-listing-switches "-alh --group-directories-first -v"
                  dired-create-destination-dirs 'ask
                  dired-create-destination-dirs-on-trailing-dirsep t
                  dired-mouse-drag-files t
                  dired-find-subdir t)
            (advice-add 'dired-up-directory :before
                        (lambda (&rest _)
                          "Leave the mark upon leaving an inserted subdir."
                          (push-mark)))
            (advice-add 'dnd-begin-drag-files :filter-args
                        (lambda (args)
                          "Reorder the dragged files from top to bottom."
                          (cons (nreverse (car args)) (cdr args))))
            (when (fboundp 'dired-do-open)
              (define-key dired-mode-map (kbd "e") #'dired-do-open))))

(use-package dired-x
  :after dired
  :config (setq dired-guess-shell-alist-user
                `(("\\.\\(?:jpe?g\\|png\\)\\'"
                   . (,@(when (executable-find "sxiv")
                          '("sxiv -o *"))
                      ,@(when (executable-find "mogrify")
                          '("mogrify -auto-orient -strip"
                            "mogrify -resize 50%"))
                      ,@(when (executable-find "httpload")
                          '("httpload"))))
                  (,vifon/media-video-extension-rx
                   . (,@(when (executable-find "mpv")
                          '("mpv * &"))
                      ,@(when (executable-find "httpload")
                          '("httpload"))))
                  (,vifon/media-audio-extension-rx
                   . (,@(when (executable-find "mpv")
                          '("mpv *")))))))

(use-package dired-subtree
  :straight t
  :after dired
  :commands dired-subtree-toggle)

(use-package dired-filter
  :straight t
  :after dired
  :config (setq-default dired-filter-stack '()))

(use-package dired-rifle
  :straight t
  :after dired
  :bind ( :map dired-mode-map
          ("r" . dired-rifle)))

(use-package dired-collapse
  :straight t
  :after dired)

(use-package dired-async
  :straight async
  :after dired
  :commands dired-async-mode
  :config (add-function :after dired-async-message-function
                        (lambda (text face &rest args)
                          (call-process "notify-send" nil 0 nil
                                        "Emacs: dired-async"
                                        (apply #'format text args)))))

(defun vifon/dired-insert-subdir-keep-point ()
  (interactive)
  (let ((dir-name (dired-get-filename t)))
    (call-interactively #'dired-maybe-insert-subdir)
    (dired-jump)
    (dired-next-line 1)
    (message "Inserted `%s'" dir-name)))

(defun vifon/dired-wl-copy (&optional directly)
  "Naively copy the dired selection into the Wayland clipboard."
  (interactive "P")
  (let ((selected-files (dired-get-marked-files nil nil nil nil t)))
    (if directly
        (dired-do-shell-command "wl-copy < ? 2> /dev/null" nil selected-files)
      (let ((file-uris (mapconcat (lambda (f) (concat "file://" f))
                                  selected-files "\n")))
        (call-process "wl-copy" nil nil nil
                      "-t" "text/uri-list" file-uris))))
  (message "Files copied to the Wayland clipboard"))

(defun vifon/dired-move-to-filename ()
  "Just like `dired-move-to-filename' but interactive."
  (interactive)
  (dired-move-to-filename))

(defmacro vifon/with-dired-directory (&rest body)
  "Respect `dired-current-directory' when applicable.

Evaluate BODY using `dired-current-directory' as
`default-directory' when it's applicable."
  (declare (indent 0))
  `(let ((default-directory (if (derived-mode-p 'dired-mode)
                                (dired-current-directory)
                              default-directory)))
     ,@body))

(defun vifon/call-with-dired-directory (func &rest args)
  "Respect `dired-current-directory' when applicable.

Call FUNC with ARGS using `dired-current-directory' as
`default-directory' when it's applicable."
  (vifon/with-dired-directory (apply func args)))

(defun vifon/find-file-from-dired ()
  "A `find-file' wrapper aware of the inserted dired subdirs."
  (interactive)
  (vifon/with-dired-directory (call-interactively #'find-file)))

(defun vifon/dired-do-shell-command-in-eat ()
  (interactive)
  (cl-letf (((symbol-function 'shell-command) #'eat))
    (call-interactively #'dired-do-shell-command)))
