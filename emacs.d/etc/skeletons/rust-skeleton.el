;;; -*- lexical-binding: t; -*-

(define-skeleton rust-skeleton
  "" nil
  "fn main() -> Result<(), std::io::Error> {\n"
  "    " _ "\n"
  "    Ok(())\n"
  "}\n"
  )

(define-auto-insert "\\.rs\\'" #'rust-skeleton)

(provide 'rust-skeleton)
