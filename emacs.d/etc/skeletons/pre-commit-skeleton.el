;;; -*- lexical-binding: t; -*-

(defun pre-commit-insert-template ()
  "Insert a personalized pre-commit config template."
  (interactive)
  (insert
   (with-temp-buffer
     ;; Insert the vanilla template
     (call-process "pre-commit" nil t nil "sample-config")

     ;; Reformat to my preferred style.
     (goto-char (point-min))
     (while (re-search-forward "   \\([a-z]\\)" nil t)
       (replace-match " \\1"))

     (buffer-string)))
  (when (y-or-n-p "Insert the trivial unittest runner?")
    (insert "\n")
    (pre-commit-local-tests-skeleton)))

(define-skeleton pre-commit-local-tests-skeleton
  "" nil
  "- repo: local\n"
  "  hooks:\n"
  "    - id: unittests\n"
  "      name: Unittests\n"
  "      language: script\n"
  "      entry: ./run.sh" _ "\n"
  "      pass_filenames: false\n"
  )

(define-auto-insert "/\\.pre-commit-config\\.yaml\\'" #'pre-commit-insert-template)

(provide 'pre-commit-skeleton)
