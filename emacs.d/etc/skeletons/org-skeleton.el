;;; -*- lexical-binding: t; -*-

(define-skeleton org-todo-skeleton
  "" nil
  "#+TODO: TODO(t) MAYBE(m) | INTEGRATION(i!) WAITING(w@) DONE(d!/@) ABORTED(a@/@)\n\n"
  _
  )

(define-skeleton org-timetable-skeleton
  "" nil
  "* Timetable

** Monthly
   #+BEGIN: clocktable :maxlevel 4 :scope file :block thismonth
   #+END:

** Weekly
   #+BEGIN: clocktable :maxlevel 4 :scope file :block thisweek
   #+END:

** Daily
   #+BEGIN: clocktable :maxlevel 4 :scope file :block thisweek :step day
   #+END:")

(provide 'org-skeleton)
