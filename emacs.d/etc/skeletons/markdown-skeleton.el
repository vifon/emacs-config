;;; -*- lexical-binding: t; -*-

(define-skeleton markdown-readme-skeleton
  "A readme template."
  (file-name-nondirectory
   (directory-file-name
    (file-name-directory
     (buffer-file-name))))
  str "\n"
  (make-string (length str) ?=)
  "\n\n"

  "SYNOPSIS\n"
  "--------\n\n"

  _ & "\n" "\n"

  "DESCRIPTION\n"
  "-----------\n\n"

  _ & "\n" "\n"

  "AUTHOR\n"
  "------\n\n"

  user-full-name "\n\n"

  "COPYRIGHT\n"
  "---------\n\n"

  "Copyright (C) " (format-time-string "%Y") "  " user-full-name "

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
  )

(define-auto-insert "/README\\.md\\'" #'markdown-readme-skeleton)


(provide 'markdown-skeleton)
