;;; -*- lexical-binding: t; -*-

(define-skeleton desktop-file-skeleton
  "" (read-string "Program name: " (file-name-base (buffer-file-name)))
  "[Desktop Entry]\n"
  "Name=" str "\n"
  "Exec=" (downcase str) _ "\n"
  "# Icon=\n"
  "# Categories=Network;\n"
  "Terminal=true\n"
  "Type=Application\n"
  "StartupNotify=false\n"
  "# StartupWMClass=\n"
  )
(define-auto-insert "\\.desktop\\'" #'desktop-file-skeleton)

(define-skeleton desktop-action-skeleton
  "" "Action name: "
  "Actions=" str ";\n"
  "\n"
  "[Desktop Action " str "]\n"
  "Name=" str "\n"
  "Exec=" str _ "\n"
  )

(provide 'misc-skeletons)
