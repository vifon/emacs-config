;;; -*- lexical-binding: t; -*-

(define-skeleton systemd-service-skeleton
  "" nil
  "[Unit]\n"
  "Description=" (file-name-base (buffer-file-name)) "\n"
  "After=network.target\n"
  "\n"
  "[Service]\n"
  "#User=root\n"
  "#WorkingDirectory=\n"
  "ExecStart=" _ "\n"
  "#ExecReload=kill -HUP $MAINPID\n"
  "#Restart=on-failure\n"
  "\n"
  "[Install]\n"
  "WantedBy=multi-user.target\n"
  )

(define-auto-insert "\\.service\\'" #'systemd-service-skeleton)

(defun browse-systemd-service-manual ()
  (interactive)
  (eww "https://www.freedesktop.org/software/systemd/man/systemd.service.html"))

(provide 'systemd-skeleton)
