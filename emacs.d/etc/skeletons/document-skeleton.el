;;; -*- lexical-binding: t; -*-

(define-skeleton madr-skeleton
  "Markdown Architectural Decision Record.

Uses the file name as the document title and the marked region as
the description."
  nil
  "# " (skeleton-read
        "Title: "
        (let ((name (file-name-sans-extension
                     (file-name-nondirectory
                      (buffer-file-name)))))
          (if (string-match "[[:digit:]]\\{4\\}-\\([a-z]\\)\\(.*\\)" name)
              (concat (upcase (match-string 1 name))
                      (string-replace "-" " " (match-string 2 name)))
            name)))
  "

## Context and Problem Statement

" _ | "{Describe the context and problem statement, e.g., in free form using two to three sentences or in the form of an illustrative story. You may want to articulate the problem in form of a question and add links to collaboration boards or issue management systems.}"
  "

## Decision Drivers  <!-- optional -->

"
  ("Decision driver: " "* " str "\n") & "\n" | -40
  "## Considered Options

"
  '(setq v1 nil)
  ("Considered option: " "* " str '(push str v1) "\n") & "\n"
  '(setq v1 (mapcar (lambda (x) (string-trim-right x "[,.;]+"))
                    (reverse v1)))
  "## Decision Outcome

Chosen option: "
  "\"" (completing-read "Chosen option: " v1) | "{title of option 1}" "\""
  ", because {justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force {force} | … | comes out best (see below)}.

### Consequences  <!-- optional -->

* Good, because {positive consequence, e.g., improvement of one or more desired qualities, …}
* Bad, because {negative consequence, e.g., compromising one or more desired qualities, …}
* … <!-- numbers of consequences can vary -->

### Confirmation  <!-- optional -->

{Describe how the implementation / compliance of the ADR can/will be confirmed. Is there any automated or manual fitness function? If so, list it and explain how it is applied. Is the chosen design and its implementation in line with the decision? E.g., a design/code review or a test with a library such as ArchUnit can help validate this. Note that although we classify this element as optional, it is included in many ADRs.}

" "## Pros and Cons of the Options  <!-- optional -->

" (when (car v1) '(nil "### " (car v1) "

<!-- This is an optional element. Feel free to remove. -->
{example | description | pointer to more information | …}

* Good, because {argument a}
* Good, because {argument b}
<!-- use \"neutral\" if the given argument weights neither for good nor bad -->
* Neutral, because {argument c}
* Bad, because {argument d}
* … <!-- numbers of pros and cons can vary -->

")) | -52
  `(,(cdr v1) "### " str "

* Good, because {argument a}
* Bad, because {argument b}

")
  "## More Information  <!-- optional -->

{You might want to provide additional evidence/confidence for the decision outcome here and/or document the team agreement on the decision and/or define when/how this decision the decision should be realized and if/when it should be re-visited. Links to other decisions and resources might appear here as well.}
"
  )

(define-auto-insert "/docs/decisions/[^/]+\\.md\\'" #'madr-skeleton)


(define-skeleton madr-minimal-skeleton
  "Markdown Architectural Decision Record, a minimal version."
  nil
  "# " (skeleton-read
        "Title: "
        (let ((name (file-name-sans-extension
                     (file-name-nondirectory
                      (buffer-file-name)))))
          (if (string-match "[[:digit:]]\\{4\\}-\\([a-z]\\)\\(.*\\)" name)
              (concat (upcase (match-string 1 name))
                      (string-replace "-" " " (match-string 2 name)))
            name)))
  "

## Context and Problem Statement

" _ | "{Describe the context and problem statement, e.g., in free form using two to three sentences or in the form of an illustrative story. You may want to articulate the problem in form of a question and add links to collaboration boards or issue management systems.}"
  "

## Considered Options

"
  ("Considered option: " "* " str "\n") & "\n"
  "## Decision Outcome

Chosen option: \"{title of option 1}\", because {justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force {force} | … | comes out best (see below)}.
"
  )


(provide 'document-skeleton)
