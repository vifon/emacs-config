;;; -*- lexical-binding: t; -*-

(define-skeleton ansible-main-skeleton
  "" nil
  "#!/usr/bin/env ansible-playbook\n"
  "\n"
  "- hosts: all\n"
  "  tasks:\n"
  "    - name: Install Emacs\n"
  "      ansible.builtin.package:\n"
  "        name: emacs-nox\n"
  "        state: present\n"
  )
(define-auto-insert "/main\\.yml\\'" #'ansible-main-skeleton)

(define-skeleton ansible-conf-skeleton
  "" nil
  "[defaults]\n"
  "inventory = hosts\n"
  "host_key_checking = True\n"
  "stdout_callback = yaml\n"
  )
(define-auto-insert "/ansible\\.cfg\\'" #'ansible-conf-skeleton)

(define-skeleton ansible-hosts-skeleton
  "" nil
  "[example-group]\n"
  "example.com\n"
  )
;; No auto-insert on purpose.  The intended filename is far too generic.

;; Ugly but should be perfectly usable.
(defun vifon/ansible-init ()
  "Create stubs of typical Ansible files in the current directory."
  (interactive)

  (find-file "./ansible.cfg")
  (ansible-conf-skeleton)
  (save-buffer)

  (find-file-other-window "./hosts")
  (ansible-hosts-skeleton)
  (save-buffer)

  (find-file-other-window "./main.yml")
  (ansible-main-skeleton)
  (save-buffer))

(provide 'ansible-skeletons)
