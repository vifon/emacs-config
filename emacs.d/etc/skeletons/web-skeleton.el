;;; -*- lexical-binding: t; -*-

(define-skeleton html-skeleton
  "" nil
  "<!DOCTYPE html>\n"
  "<html lang=\"en\">\n"
  "  <head>\n"
  "    <meta charset=\"utf-8\">\n"
  "    <title></title>\n"
  "    <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">\n"
  "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
  "  </head>\n"
  "\n"
  "  <body>\n"
  "    " _ "\n"
  "  </body>\n"
  "</html>\n"
  )

(define-auto-insert "\\.html\\'" #'html-skeleton)

(provide 'web-skeleton)
