;;; -*- lexical-binding: t; -*-

(define-skeleton docker-alpine-skeleton
  "" nil
  "FROM alpine\n"
  "\n"
  "RUN apk --no-cache add " _ "\n"
  "\n"
  "RUN adduser -D app\n"
  "USER app\n"
  "WORKDIR /home/app\n"
  "\n"
  "EXPOSE 5000\n"
  "\n"
  "CMD [\"/bin/sh\"]\n"
  )

(define-skeleton docker-debian-skeleton
  "" nil
  "FROM debian\n"
  "\n"
  "RUN apt-get update && \\\n"
  "    apt-get install --no-install-recommends -y " _ " && \\\n"
  "    apt-get clean && rm -rf /var/lib/apt/lists/*\n"
  "\n"
  "CMD [\"/bin/sh\"]\n"
  )

(define-skeleton docker-compose-skeleton
  "" nil
  "services:\n"
  "  app:\n"
  "    image: alpine:latest\n"
  "    volumes:\n"
  "      - type: bind\n"
  "        source: .\n"
  "        target: /src\n"
  "        read_only: true\n"
  "    working_dir: /src\n"
  "    command: true" _ "\n"
  )

(define-skeleton docker-compose-build-skeleton
  "" nil
  "services:\n"
  "  app:\n"
  "    image: "
  user-login-name "/" (file-name-nondirectory
                       (directory-file-name
                        default-directory))
  ":latest\n"
  "    build: .\n"
  )


(defalias 'docker-skeleton 'docker-alpine-skeleton)

(define-auto-insert "/Dockerfile\\'" #'docker-skeleton)
(define-auto-insert "/docker-compose.ya?ml\\'" #'docker-compose-skeleton)
(define-auto-insert "/compose.ya?ml\\'" #'docker-compose-skeleton)

(provide 'docker-skeleton)
