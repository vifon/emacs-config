;;; -*- lexical-binding: t; -*-

(define-skeleton bash-strict-skeleton
  "" nil
  "#!/usr/bin/env bash\n"
  "\n"
  "SCRIPTDIR=\"$(cd \"$(dirname \"${BASH_SOURCE[0]}\")\" > /dev/null 2>&1 && pwd)\"\n"
  "\n"
  "set -o errexit -o nounset -o pipefail\n"
  "\n"
  _
  )

;; Note: do not trap the EXIT "signal", it might get executed multiple
;; times.  An explicit call after the body seems more reliable.
(define-skeleton bash-cleanup-skeleton
  "" nil
  "finish() {\n"
  "    EXITCODE=\"$?\"\n"
  "    \n"
  "    exit \"$EXITCODE\"\n"
  "}; trap finish ERR INT TERM\n"
  "\n"
  _ "\n"
  "\n"
  "finish\n"
  )

(define-skeleton shell-runner-skeleton
  "A Makefile-like command runner." ""
  "#!/usr/bin/env bash\n"
  "# shellcheck disable=SC2317\n"
  "\n"
  "SCRIPTDIR=\"$(cd \"$(dirname \"${BASH_SOURCE[0]}\")\" > /dev/null 2>&1 && pwd)\"\n"
  "cd \"$SCRIPTDIR\"\n"
  "\n"
  "set -o errexit -o nounset -o pipefail\n"
  "\n"
  "help() {\n"
  "    completion 2> /dev/null\n"
  "}\n"
  "\n"
  "completion() {\n"
  "    compgen -A function || typeset -f | awk '/ \\(\\) $/ { print $1 }'\n"
  "}\n"
  "\n"
  "all() {\n"
  "    echo \"Nothing to do…\"\n"
  "}\n"
  "\n"
  _ "\n"
  "\n"
  "\n"
  "[ \"$#\" -eq 0 ] && set -- all\n"
  "set -x\n"
  "\"$@\"; exit\n"
  )

;; Not quite perfect, as in the corner cases it causes merge conflicts
;; when re-applying the stash, but it's still a quite sane starting
;; point in general.
(define-skeleton git-hook-precommit-skeleton
  "A starting point for the Git pre-commit hooks." ""
  "#!/usr/bin/env bash\n"
  "\n"
  "set -o errexit -o nounset -o pipefail\n"
  "\n"
  "unset GIT_LITERAL_PATHSPECS\n"
  "\n"
  "git stash -q --keep-index\n"
  "cleanup () {\n"
  "    CODE=\"$?\"\n"
  "    git stash pop -q || true\n"
  "    exit \"$CODE\"\n"
  "}; trap cleanup ERR INT TERM\n"
  "\n"
  _
  "\n"
  "cleanup\n"
  )

(define-auto-insert "\\.sh\\'" #'bash-strict-skeleton)
(define-auto-insert "/\\.git/hooks/pre-commit\\'" #'git-hook-precommit-skeleton)
(define-auto-insert "/make\\.sh\\'" #'shell-runner-skeleton)
(define-auto-insert "/run\\.sh\\'" #'shell-runner-skeleton)

(provide 'shell-skeleton)
