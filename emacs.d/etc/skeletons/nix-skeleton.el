;;; -*- lexical-binding: t; -*-

(define-skeleton nix-shell-skeleton
  "" nil
  "{ pkgs ? import <nixpkgs> {} }:\n"
  "\n"
  "with pkgs; mkShell {\n"
  "  buildInputs = [\n"
  "    " _ "\n"
  "  ];\n"
  "  # shellHook = ''\n"
  "  # '';\n"
  "}\n"
  )

(define-skeleton nix-shell-python-skeleton
  "" nil
  "{ pkgs ? import <nixpkgs> {} }:\n"
  "\n"
  "with pkgs; mkShell {\n"
  "  buildInputs = let pyenv = ps: with ps; [\n"
  "    " _ "\n"
  "  ]; in [\n"
  "    (python3.withPackages pyenv)\n"
  "  ];\n"
  "}\n"
  )

(define-skeleton nix-flake-skeleton
  "" nil
  "{\n"
  "  # description = \"...\";\n"
  "\n"
  "  inputs = {\n"
  "    nixpkgs.url = \"github:nixos/nixpkgs?ref=nixos-unstable\";\n"
  "  };\n"
  "\n"
  "  outputs = { self, nixpkgs }:\n"
  "    let pkgs = import nixpkgs { system = \"x86_64-linux\"; };\n"
  "    in {\n"
  "      # packages.x86_64-linux = {\n"
  "      #   default = pkgs.callPackage ./default.nix {};\n"
  "      # };\n"
  "      devShell.x86_64-linux = pkgs.mkShell rec {\n"
  "        buildInputs = with pkgs; [\n"
  "          " _ "\n"
  "          # (python3.withPackages (ps: with ps; [ ]))\n"
  "        ];\n"
  "      };\n"
  "    };\n"
  "}\n"
  )

(define-skeleton nix-docker-skeleton
  "" nil
  "{ pkgs ? import <nixpkgs> {} }:\n"
  "\n"
  "with pkgs;\n"
  "dockerTools.buildImage {\n"
  "  name = \"" (skeleton-read "Image name: "
                                (file-name-nondirectory
                                 (directory-file-name
                                  default-directory))) "\";\n"
  "  tag = \"latest\";\n"
  "\n"
  "  contents = [ coreutils" _ " ];\n"
  "\n"
  "  config = {\n"
  "  Cmd = [ \"${bash}/bin/bash\" ];\n"
  "  };\n"
  "}\n"
  )

(define-skeleton nix-package-skeleton
  "" nil
  '(setq v1
         (if (y-or-n-p "Fetch from the web?")
             (if (y-or-n-p "Fetch from Github?")
                 'github
               'web)
           nil))
  (if (y-or-n-p "Pass a full argument list?")
      (concat "{ stdenv, lib, pkg-config"
              (pcase v1
                ('github ", fetchFromGithub")
                ('web ", fetchurl"))
              " }:\n\n")
    (concat "{ pkgs ? import <nixpkgs> {} }:\n"
            "\n"
            "with pkgs;\n"))
  "stdenv.mkDerivation rec {\n"
  "  pname = \"" (file-name-nondirectory
                  (directory-file-name
                   default-directory))
  "\";\n"
  "  version = \"0.9\";\n"
  (pcase v1
    ('github
     (concat "  src = fetchFromGitHub {\n"
             "    owner = \"" (skeleton-read "Repo owner: " (user-login-name)) "\";\n"
             "    repo = pname;\n"
             "    rev = \"v${version}\";\n"
             "    sha256 = \"\";\n"
             "  };\n"))
    ('web
     (concat "  src = fetchurl {\n"
             "    url = " (setq v2 (skeleton-read "Fetch URL: ")) ";\n"
             "    sha256 = \"" (when (and (not (string-empty-p v2))
                                          (y-or-n-p "Fetch & calculate the hash?"))
                                 (shell-command-to-string (concat "nix-prefetch-url "
                                                                  "\"" v2 "\""
                                                                  " 2> /dev/null"))) "\";\n"
             "  };\n"))
    (_ "  src = lib.cleanSource ./.;\n"))
  "\n"
  "  buildInputs = [\n"
  "    pkg-config" _ "\n"
  "  ];\n"
  (when (y-or-n-p "Add a custom installPhase?")
    (let ((pname (file-name-nondirectory
                  (directory-file-name
                   default-directory))))
      (concat "\n"
              "  installPhase = ''\n"
              "    install -D -m755 " pname " $out/bin/" pname "\n"
              "  '';\n")))
  "}\n"
  )

(define-skeleton nix-module-skeleton
  "" nil
  "{ config, pkgs, lib, ... }:\n"
  "\n"
  "let cfg = config.services." (setq v1 (skeleton-read "Module name: ")) ";\n"
  "in {\n"
  "  options = {\n"
  "    services." v1 " = {\n"
  "      enable = lib.mkOption {\n"
  "        default = false;\n"
  "        type = with lib.types; bool;\n"
  "        description = ''\n"
  "        '';\n"
  "      };\n"
  "    };\n"
  "  };\n"
  "\n"
  "  config = lib.mkIf cfg.enable {\n"
  "    environment.systemPackages = [ " _ "];\n"
  "  };\n"
  "}\n"
  )

(define-auto-insert "/shell\\.nix\\'" #'nix-shell-skeleton)
(define-auto-insert "/default\\.nix\\'" #'nix-package-skeleton)
(define-auto-insert "/flake\\.nix\\'" #'nix-flake-skeleton)

(provide 'nix-skeleton)
