;;; -*- lexical-binding: t; -*-

(define-skeleton haskell-skeleton
  "" nil
  "module Main where\n\n"
  "main :: IO ()\n"
  "main = do\n"
  "  " _ "\n"
  "  return ()\n"
  )

(define-auto-insert "\\.hs\\'" #'haskell-skeleton)

(provide 'haskell-skeleton)
